/*
Navicat SQL Server Data Transfer

Source Server         : Sistema_Clinica
Source Server Version : 120000
Source Host           : localhost:1433
Source Database       : Sistema_Clinica
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 120000
File Encoding         : 65001

Date: 2014-11-22 02:42:30
*/


-- ----------------------------
-- Table structure for Medicos
-- ----------------------------
DROP TABLE [dbo].[Medicos]
GO
CREATE TABLE [dbo].[Medicos] (
[ID_Medico] int NOT NULL IDENTITY(1,1) ,
[Apellido] nvarchar(MAX) NOT NULL ,
[Nombre] nvarchar(MAX) NOT NULL ,
[DNI] varchar(10) NOT NULL ,
[Nro_Matricula] nvarchar(MAX) NOT NULL ,
[Fecha_Nacimiento] date NOT NULL ,
[Domicilio] nvarchar(MAX) NOT NULL ,
[Telefono] nvarchar(MAX) NOT NULL ,
[Actividad] bit NOT NULL DEFAULT ((1)) ,
[Observaciones] nvarchar(MAX) NULL ,
[Contraseña] char(64) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Medicos]', RESEED, 3)
GO

-- ----------------------------
-- Records of Medicos
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Medicos] ON
GO
INSERT INTO [dbo].[Medicos] ([ID_Medico], [Apellido], [Nombre], [DNI], [Nro_Matricula], [Fecha_Nacimiento], [Domicilio], [Telefono], [Actividad], [Observaciones], [Contraseña]) VALUES (N'3', N'Pezzani', N'Agustin', N'37559909', N'dwhwEK0svgPKow4Ek18NxtBoyR7UFKx46Yxzftt5eohwKQX69ITBr0+yN0M08dGaYUG7EgJvvMw2wBFb97u50V6mJ8NJWDEv6PXRhio/2Q9+qevW3YKrCCOf8Rd5nJzWSZ3CITDujt7x+xSSJcdnvYBAT2P5eGWlUExLKCSILWw=', N'1993-05-10', N'118 e 527 y 528', N'588123', N'1', N'', N'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f')
GO
GO
SET IDENTITY_INSERT [dbo].[Medicos] OFF
GO

-- ----------------------------
-- Table structure for Operarios
-- ----------------------------
DROP TABLE [dbo].[Operarios]
GO
CREATE TABLE [dbo].[Operarios] (
[ID_Operario] int NOT NULL IDENTITY(1,1) ,
[Apellido] nvarchar(MAX) NOT NULL ,
[Nombre] nvarchar(MAX) NOT NULL ,
[DNI] varchar(10) NOT NULL ,
[Fecha_Nacimiento] date NOT NULL ,
[Domicilio] nvarchar(MAX) NOT NULL ,
[Telefono] nvarchar(MAX) NOT NULL ,
[Actividad] bit NOT NULL DEFAULT ((1)) ,
[Observaciones] nvarchar(MAX) NULL ,
[Contraseña] char(64) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Operarios]', RESEED, 3)
GO

-- ----------------------------
-- Records of Operarios
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Operarios] ON
GO
INSERT INTO [dbo].[Operarios] ([ID_Operario], [Apellido], [Nombre], [DNI], [Fecha_Nacimiento], [Domicilio], [Telefono], [Actividad], [Observaciones], [Contraseña]) VALUES (N'3', N'Kravetz', N'Daniel', N'36683820', N'1992-01-14', N'116 nº 365', N'4834183', N'1', N'', N'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f')
GO
GO
SET IDENTITY_INSERT [dbo].[Operarios] OFF
GO

-- ----------------------------
-- Table structure for Pacientes
-- ----------------------------
DROP TABLE [dbo].[Pacientes]
GO
CREATE TABLE [dbo].[Pacientes] (
[ID_Paciente] int NOT NULL IDENTITY(1,1) ,
[Apellido] nvarchar(MAX) NOT NULL ,
[Nombre] nvarchar(MAX) NOT NULL ,
[DNI] varchar(10) NOT NULL ,
[Fecha_Nacimiento] date NOT NULL ,
[Domicilio] nvarchar(MAX) NOT NULL ,
[Telefono] nvarchar(MAX) NOT NULL ,
[Actividad] bit NOT NULL DEFAULT ((1)) ,
[Observaciones] nvarchar(MAX) NULL ,
[ID_Operario] int NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Pacientes]', RESEED, 3)
GO

-- ----------------------------
-- Records of Pacientes
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Pacientes] ON
GO
INSERT INTO [dbo].[Pacientes] ([ID_Paciente], [Apellido], [Nombre], [DNI], [Fecha_Nacimiento], [Domicilio], [Telefono], [Actividad], [Observaciones], [ID_Operario]) VALUES (N'3', N'Soliz', N'Iván', N'36683820', N'1991-10-17', N'520 y 19', N'0303456', N'1', N'Es alto', N'3')
GO
GO
SET IDENTITY_INSERT [dbo].[Pacientes] OFF
GO

-- ----------------------------
-- Table structure for Turnos
-- ----------------------------
DROP TABLE [dbo].[Turnos]
GO
CREATE TABLE [dbo].[Turnos] (
[ID_Turno] int NOT NULL IDENTITY(1,1) ,
[ID_Operario] int NOT NULL ,
[ID_Paciente] int NOT NULL ,
[ID_Medico] int NOT NULL ,
[Fecha_Turno] date NOT NULL ,
[Horario_Turno] nvarchar(MAX) NOT NULL ,
[Motivo_Consulta] nvarchar(MAX) NOT NULL ,
[Actividad] bit NOT NULL DEFAULT ((1)) ,
[Observaciones] nvarchar(MAX) NULL ,
[Asistencia] bit NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Turnos]', RESEED, 3)
GO

-- ----------------------------
-- Records of Turnos
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Turnos] ON
GO
SET IDENTITY_INSERT [dbo].[Turnos] OFF
GO

-- ----------------------------
-- Procedure structure for getMedicos
-- ----------------------------
DROP PROCEDURE [dbo].[getMedicos]
GO






CREATE PROCEDURE [dbo].[getMedicos]
AS
BEGIN
  SELECT * From dbo.Medicos
where Actividad = 1
END






GO

-- ----------------------------
-- Procedure structure for getMedicosHistorial
-- ----------------------------
DROP PROCEDURE [dbo].[getMedicosHistorial]
GO



CREATE PROCEDURE [dbo].[getMedicosHistorial]
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * From dbo.Medicos
END



GO

-- ----------------------------
-- Procedure structure for getOperarios
-- ----------------------------
DROP PROCEDURE [dbo].[getOperarios]
GO






CREATE PROCEDURE [dbo].[getOperarios]
AS
BEGIN
  SELECT * from dbo.Operarios
WHERE Actividad = 1
END






GO

-- ----------------------------
-- Procedure structure for getOperariosHistorial
-- ----------------------------
DROP PROCEDURE [dbo].[getOperariosHistorial]
GO



CREATE PROCEDURE [dbo].[getOperariosHistorial]
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * From dbo.Operarios
END



GO

-- ----------------------------
-- Procedure structure for getPacientes
-- ----------------------------
DROP PROCEDURE [dbo].[getPacientes]
GO






CREATE PROCEDURE [dbo].[getPacientes]
AS
BEGIN
  SELECT * from dbo.Pacientes
where Actividad = 1
END






GO

-- ----------------------------
-- Procedure structure for getPacientesHistorial
-- ----------------------------
DROP PROCEDURE [dbo].[getPacientesHistorial]
GO



CREATE PROCEDURE [dbo].[getPacientesHistorial]
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * From dbo.Pacientes
END



GO

-- ----------------------------
-- Procedure structure for getTurnos
-- ----------------------------
DROP PROCEDURE [dbo].[getTurnos]
GO


CREATE PROCEDURE [dbo].[getTurnos]
@FechaInicial as date,
@FechaFinal as date 
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Turnos where Turnos.Actividad = 1
	and Turnos.Fecha_Turno >= @FechaInicial and Turnos.Fecha_Turno <= @FechaFinal
END



GO

-- ----------------------------
-- Procedure structure for getTurnosMedico
-- ----------------------------
DROP PROCEDURE [dbo].[getTurnosMedico]
GO








CREATE PROCEDURE [dbo].[getTurnosMedico]
  @ID_Medico AS int 
AS
BEGIN
SELECT
dbo.Turnos.ID_Turno,
dbo.Pacientes.Apellido,
dbo.Pacientes.Nombre,
dbo.Turnos.Fecha_Turno as "Fecha Turno",
dbo.Turnos.Horario_Turno as "Horario Turno",
dbo.Turnos.Motivo_Consulta as "Motivo Consulta",
dbo.Turnos.Actividad,
dbo.Turnos.Observaciones,
dbo.Turnos.Asistencia,
dbo.Pacientes.ID_Paciente


FROM
dbo.Turnos
INNER JOIN dbo.Pacientes ON dbo.Turnos.ID_Paciente = dbo.Pacientes.ID_Paciente
WHERE dbo.Turnos.ID_Medico = @ID_Medico AND
dbo.Turnos.Actividad = 1 AND
dbo.Turnos.Fecha_Turno >= CONVERT (date, GETDATE())
END








GO

-- ----------------------------
-- Procedure structure for getTurnosMedicoByDate
-- ----------------------------
DROP PROCEDURE [dbo].[getTurnosMedicoByDate]
GO







CREATE PROCEDURE [dbo].[getTurnosMedicoByDate]
	@ID_Medico AS int,
  @Fecha_Turno AS date
AS
BEGIN
SELECT
dbo.Turnos.ID_Turno,
dbo.Turnos.Horario_Turno as "Horario Turno",
dbo.Pacientes.Apellido,
dbo.Pacientes.Nombre,
dbo.Turnos.Motivo_Consulta as "Motivo Consulta",
dbo.Turnos.Observaciones,
dbo.Pacientes.ID_Paciente


FROM
dbo.Turnos
INNER JOIN dbo.Pacientes ON dbo.Turnos.ID_Paciente = dbo.Pacientes.ID_Paciente
WHERE dbo.Turnos.ID_Medico = @ID_Medico AND
dbo.Turnos.Fecha_Turno = @Fecha_Turno AND
dbo.Turnos.Actividad = 1
END







GO

-- ----------------------------
-- Procedure structure for getTurnosPaciente
-- ----------------------------
DROP PROCEDURE [dbo].[getTurnosPaciente]
GO








CREATE PROCEDURE [dbo].[getTurnosPaciente]
  @ID_Paciente AS int 
AS
BEGIN
SELECT
dbo.Turnos.Fecha_Turno as "Fecha Turno",
dbo.Turnos.Horario_Turno as "Horario Turno",
dbo.Medicos.Apellido,
dbo.Medicos.Nombre,
dbo.Turnos.Motivo_Consulta as "Motivo Consulta",
dbo.Turnos.Asistencia,
dbo.Turnos.Observaciones

FROM
dbo.Pacientes
INNER JOIN dbo.Turnos ON dbo.Turnos.ID_Paciente = dbo.Pacientes.ID_Paciente
INNER JOIN dbo.Medicos ON dbo.Turnos.ID_Medico = dbo.Medicos.ID_Medico
WHERE
dbo.Pacientes.ID_Paciente = @ID_Paciente AND
dbo.Turnos.Actividad = 1
END








GO

-- ----------------------------
-- Indexes structure for table Medicos
-- ----------------------------
CREATE UNIQUE INDEX [DNI] ON [dbo].[Medicos]
([DNI] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO
CREATE UNIQUE INDEX [ID_Medico] ON [dbo].[Medicos]
([ID_Medico] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO

-- ----------------------------
-- Primary Key structure for table Medicos
-- ----------------------------
ALTER TABLE [dbo].[Medicos] ADD PRIMARY KEY ([ID_Medico])
GO

-- ----------------------------
-- Triggers structure for table Medicos
-- ----------------------------
DROP TRIGGER [dbo].[trgInsertMedico]
GO
CREATE TRIGGER [dbo].[trgInsertMedico]
ON [dbo].[Medicos]
INSTEAD OF INSERT
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    insert into Medicos (Nombre, Apellido, DNI, Fecha_Nacimiento, Domicilio, Telefono, Actividad, Observaciones, Contraseña, Nro_Matricula)
	SELECT Nombre, Apellido, DNI, Fecha_Nacimiento, Domicilio, Telefono, 1, Observaciones, Contraseña, Nro_Matricula
	from inserted
    -- Insert statements for trigger here

END



GO

-- ----------------------------
-- Indexes structure for table Operarios
-- ----------------------------
CREATE UNIQUE INDEX [DNI] ON [dbo].[Operarios]
([DNI] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO
CREATE UNIQUE INDEX [ID_Operario] ON [dbo].[Operarios]
([ID_Operario] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO

-- ----------------------------
-- Primary Key structure for table Operarios
-- ----------------------------
ALTER TABLE [dbo].[Operarios] ADD PRIMARY KEY ([ID_Operario])
GO

-- ----------------------------
-- Triggers structure for table Operarios
-- ----------------------------
DROP TRIGGER [dbo].[trgInsertOperario]
GO
CREATE TRIGGER [dbo].[trgInsertOperario]
ON [dbo].[Operarios]
INSTEAD OF INSERT
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	insert into Operarios(Nombre, Apellido, DNI, Fecha_Nacimiento, Domicilio, Telefono, Actividad, Observaciones, Contraseña)
	SELECT Nombre, Apellido, DNI, Fecha_Nacimiento, Domicilio, Telefono, 1, Observaciones, Contraseña
	from inserted
    -- Insert statements for trigger here

END



GO

-- ----------------------------
-- Indexes structure for table Pacientes
-- ----------------------------
CREATE UNIQUE INDEX [DNI] ON [dbo].[Pacientes]
([DNI] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO
CREATE INDEX [ID_Paciente] ON [dbo].[Pacientes]
([ID_Paciente] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Pacientes
-- ----------------------------
ALTER TABLE [dbo].[Pacientes] ADD PRIMARY KEY ([ID_Paciente])
GO

-- ----------------------------
-- Triggers structure for table Pacientes
-- ----------------------------
DROP TRIGGER [dbo].[trgInsertPaciente]
GO
CREATE TRIGGER [dbo].[trgInsertPaciente]
ON [dbo].[Pacientes]
INSTEAD OF INSERT
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	insert into Pacientes(Nombre, Apellido, DNI, Fecha_Nacimiento, Domicilio, Telefono, Actividad, Observaciones, ID_Operario)
	SELECT Nombre, Apellido, DNI, Fecha_Nacimiento, Domicilio, Telefono, 1, Observaciones, ID_Operario
	from inserted
    -- Insert statements for trigger here

END



GO

-- ----------------------------
-- Indexes structure for table Turnos
-- ----------------------------
CREATE UNIQUE INDEX [ID_Turno] ON [dbo].[Turnos]
([ID_Turno] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO
CREATE INDEX [IndexFecha] ON [dbo].[Turnos]
([Fecha_Turno] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Turnos
-- ----------------------------
ALTER TABLE [dbo].[Turnos] ADD PRIMARY KEY ([ID_Turno])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Pacientes]
-- ----------------------------
ALTER TABLE [dbo].[Pacientes] ADD FOREIGN KEY ([ID_Operario]) REFERENCES [dbo].[Operarios] ([ID_Operario]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Turnos]
-- ----------------------------
ALTER TABLE [dbo].[Turnos] ADD FOREIGN KEY ([ID_Medico]) REFERENCES [dbo].[Medicos] ([ID_Medico]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Turnos] ADD FOREIGN KEY ([ID_Operario]) REFERENCES [dbo].[Operarios] ([ID_Operario]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Turnos] ADD FOREIGN KEY ([ID_Paciente]) REFERENCES [dbo].[Pacientes] ([ID_Paciente]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
