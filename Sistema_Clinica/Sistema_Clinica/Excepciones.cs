﻿using System;
using System.Runtime.Serialization;

namespace Sistema_Clinica
{
    class Excepciones : Exception
    { }

    [Serializable]
    class UsuarioExistente : Exception, ISerializable
    {
        public UsuarioExistente()
        { }

        public UsuarioExistente(string message)
        : base(message)
        { }

        public UsuarioExistente(string message, Exception inner)
        : base(message, inner)
        { }
        protected UsuarioExistente(SerializationInfo info, StreamingContext context): base(info, context)
        { }
    }

    [Serializable]
    class ContraseñaNoCoincide : Exception, ISerializable
    {
        public ContraseñaNoCoincide()
        { }

        public ContraseñaNoCoincide(string message) : base (message)
        { }
        public ContraseñaNoCoincide(string message, Exception inner)
        : base(message, inner)
        { }
        protected ContraseñaNoCoincide(SerializationInfo info, StreamingContext context): base(info, context)
        { }
    }

    [Serializable]
    class ContraseñaNoRespetaFormato : Exception, ISerializable
    {
        public ContraseñaNoRespetaFormato()
        { }
        public ContraseñaNoRespetaFormato(string message) : base (message)
        { }
        public ContraseñaNoRespetaFormato(string message, Exception inner) : base (message, inner)
        { }
        protected ContraseñaNoRespetaFormato (SerializationInfo info, StreamingContext context): base(info, context)
        { }
    }

    [Serializable]
    class DatosPersonalesVacios : Exception, ISerializable
    {
        public DatosPersonalesVacios()
        { }
        public DatosPersonalesVacios(string message) : base (message)
        { }
        public DatosPersonalesVacios(string message, Exception inner) : base (message, inner)
        { }
        protected DatosPersonalesVacios(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class LoginFail : Exception, ISerializable
    {
        public LoginFail()
        { }

        public LoginFail(string message)
        : base(message)
        { }

        public LoginFail(string message, Exception inner)
        : base(message, inner)
        { }
        protected LoginFail(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class PacienteNoExistente : Exception, ISerializable
    {
        public PacienteNoExistente()
        { }

        public PacienteNoExistente(string message)
            : base(message)
        { }

        public PacienteNoExistente(string message, Exception inner)
            : base(message, inner)
        { }
        protected PacienteNoExistente(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class MedicoNoExistente : Exception, ISerializable
    {
        public MedicoNoExistente()
        { }

        public MedicoNoExistente(string message)
            : base(message)
        { }

        public MedicoNoExistente(string message, Exception inner)
            : base(message, inner)
        { }
        protected MedicoNoExistente(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class OperarioNoExistente : Exception, ISerializable
    {
        public OperarioNoExistente()
        { }

        public OperarioNoExistente(string message)
            : base(message)
        { }

        public OperarioNoExistente(string message, Exception inner)
            : base(message, inner)
        { }
        protected OperarioNoExistente(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class PacienteExistente : Exception, ISerializable
    {
        public PacienteExistente()
        { }

        public PacienteExistente(string message)
            : base(message)
        { }

        public PacienteExistente(string message, Exception inner)
            : base(message, inner)
        { }
        protected PacienteExistente(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class TurnoNoExistente : Exception, ISerializable
    {
        public TurnoNoExistente()
        { }

        public TurnoNoExistente(string message)
            : base(message)
        { }

        public TurnoNoExistente(string message, Exception inner)
            : base(message, inner)
        { }
        protected TurnoNoExistente(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class FechaFueraDeRango : Exception, ISerializable
    {
        public FechaFueraDeRango()
        { }

        public FechaFueraDeRango(string message)
            : base(message)
        { }

        public FechaFueraDeRango(string message, Exception inner)
            : base(message, inner)
        { }
        protected FechaFueraDeRango(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }

    [Serializable]
    class DNIEnUso : Exception, ISerializable
    {
        public DNIEnUso()
        { }

        public DNIEnUso(string message)
            : base(message)
        { }

        public DNIEnUso(string message, Exception inner)
            : base(message, inner)
        { }
        protected DNIEnUso(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
