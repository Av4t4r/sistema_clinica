﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Clinica
{
    public partial class FormularioAdministracion : Form
    {
        bool medico, operario, paciente, desencriptada = false;
        public FormularioAdministracion()
        {
            InitializeComponent();
            ActualizarDataGrids();
        }

        private void FormularioEliminarRegistros_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            { e.Cancel = true; }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ActualizarDataGrids()
        {
            dgrMedicos.DataSource = CRUD.getMedicos();
            dgrOperarios.DataSource = CRUD.getOperarios();
            dgrPacientes.DataSource = CRUD.getPacientes();

            dgrMedicos.Columns["ID_Medico"].Visible = false;
            dgrMedicos.Columns["Fecha_Nacimiento"].Visible = false;
            dgrMedicos.Columns["Domicilio"].Visible = false;
            dgrMedicos.Columns["Telefono"].Visible = false;
            dgrMedicos.Columns["Observaciones"].Visible = false;
            dgrMedicos.Columns["Contraseña"].Visible = false;
            dgrMedicos.Columns["Nro_Matricula"].Visible = false;

            dgrOperarios.Columns["ID_Operario"].Visible = false;
            dgrOperarios.Columns["Fecha_Nacimiento"].Visible = false;
            dgrOperarios.Columns["Domicilio"].Visible = false;
            dgrOperarios.Columns["Telefono"].Visible = false;
            dgrOperarios.Columns["Observaciones"].Visible = false;
            dgrOperarios.Columns["Contraseña"].Visible = false;

            dgrPacientes.Columns["ID_Paciente"].Visible = false;
            dgrPacientes.Columns["Observaciones"].Visible = false;
            dgrPacientes.Columns["Fecha_Nacimiento"].Visible = false;
            dgrPacientes.Columns["Domicilio"].Visible = false;
            dgrPacientes.Columns["Telefono"].Visible = false;
            dgrPacientes.Columns["ID_Operario"].Visible = false;
        }

        private void ActualizarDataGridsHistorial()
        {
            dgrMedicos.DataSource = CRUD.getMedicosHistorial();
            dgrOperarios.DataSource = CRUD.getOperariosHistorial();
            dgrPacientes.DataSource = CRUD.getPacientesHistorial();

            dgrMedicos.Columns["ID_Medico"].Visible = false;
            dgrMedicos.Columns["Fecha_Nacimiento"].Visible = false;
            dgrMedicos.Columns["Domicilio"].Visible = false;
            dgrMedicos.Columns["Telefono"].Visible = false;
            dgrMedicos.Columns["Observaciones"].Visible = false;
            dgrMedicos.Columns["Contraseña"].Visible = false;
            dgrMedicos.Columns["Nro_Matricula"].Visible = false;

            dgrOperarios.Columns["ID_Operario"].Visible = false;
            dgrOperarios.Columns["Fecha_Nacimiento"].Visible = false;
            dgrOperarios.Columns["Domicilio"].Visible = false;
            dgrOperarios.Columns["Telefono"].Visible = false;
            dgrOperarios.Columns["Observaciones"].Visible = false;
            dgrOperarios.Columns["Contraseña"].Visible = false;

            dgrPacientes.Columns["ID_Paciente"].Visible = false;
            dgrPacientes.Columns["Observaciones"].Visible = false;
            dgrPacientes.Columns["Fecha_Nacimiento"].Visible = false;
            dgrPacientes.Columns["Domicilio"].Visible = false;
            dgrPacientes.Columns["Telefono"].Visible = false;
            dgrPacientes.Columns["ID_Operario"].Visible = false;
        }

        private void btnEditarMedico_Click(object sender, EventArgs e)
        {
            if(dgrMedicos.CurrentRow != null)
            {
                try 
                {
                    txtApellido.Text = dgrMedicos.CurrentRow.Cells["Apellido"].Value.ToString();
                    txtNombre.Text = dgrMedicos.CurrentRow.Cells["Nombre"].Value.ToString();
                    txtDNI.Text = dgrMedicos.CurrentRow.Cells["DNI"].Value.ToString();
                    txtDomicilio.Text = dgrMedicos.CurrentRow.Cells["Domicilio"].Value.ToString();
                    txtTelefono.Text = dgrMedicos.CurrentRow.Cells["Telefono"].Value.ToString();
                    txtObservaciones.Text = dgrMedicos.CurrentRow.Cells["Observaciones"].Value.ToString();
                    dteFecha_Nacimiento.Value = DateTime.Parse(dgrMedicos.CurrentRow.Cells["Fecha_Nacimiento"].Value.ToString());
                    txtNroMatricula.Text = dgrMedicos.CurrentRow.Cells["Nro_Matricula"].Value.ToString();
                    txtID.Text = dgrMedicos.CurrentRow.Cells["ID_Medico"].Value.ToString();
                    chkActividad.Checked = (Boolean)dgrMedicos.CurrentRow.Cells["Actividad"].Value;

                    txtApellido.Enabled = true;
                    txtNombre.Enabled = true;
                    txtDNI.Enabled = true;
                    txtDomicilio.Enabled = true;
                    txtTelefono.Enabled = true;
                    txtObservaciones.Enabled = true;
                    dteFecha_Nacimiento.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnGuardarCambios.Enabled = true;
                    btnEditarContraseña.Enabled = true;
                    btnEditarMedico.Enabled = false;
                    btnEditarOperarios.Enabled = false;
                    btnEditarPaciente.Enabled = false;
                    btnDesencriptar.Enabled = true;
                    txtNroMatricula.Enabled = true;
                    if (chkActividad.Checked == true)
                    { btnEliminar.Enabled = true; }
                    medico = true;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
            else
            { MessageBox.Show("Debe elegir un medico", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnEditarOperarios_Click(object sender, EventArgs e)
        {
            if (dgrOperarios.CurrentRow != null)
            {
                try
                {
                    txtApellido.Text = dgrOperarios.CurrentRow.Cells["Apellido"].Value.ToString();
                    txtNombre.Text = dgrOperarios.CurrentRow.Cells["Nombre"].Value.ToString();
                    txtDNI.Text = dgrOperarios.CurrentRow.Cells["DNI"].Value.ToString();
                    txtDomicilio.Text = dgrOperarios.CurrentRow.Cells["Domicilio"].Value.ToString();
                    txtTelefono.Text = dgrOperarios.CurrentRow.Cells["Telefono"].Value.ToString();
                    txtObservaciones.Text = dgrOperarios.CurrentRow.Cells["Observaciones"].Value.ToString();
                    dteFecha_Nacimiento.Value = DateTime.Parse(dgrOperarios.CurrentRow.Cells["Fecha_Nacimiento"].Value.ToString());
                    txtID.Text = dgrOperarios.CurrentRow.Cells["ID_Operario"].Value.ToString();
                    chkActividad.Checked = (Boolean)dgrOperarios.CurrentRow.Cells["Actividad"].Value;

                    txtApellido.Enabled = true;
                    txtNombre.Enabled = true;
                    txtDNI.Enabled = true;
                    txtDomicilio.Enabled = true;
                    txtTelefono.Enabled = true;
                    txtObservaciones.Enabled = true;
                    dteFecha_Nacimiento.Enabled = true;
                    btnEditarMedico.Enabled = false;
                    btnEditarOperarios.Enabled = false;
                    btnEditarPaciente.Enabled = false;
                    btnEditarContraseña.Enabled = true;
                    btnGuardarCambios.Enabled = true;
                    btnCancelar.Enabled = true;
                    if (chkActividad.Checked == true)
                    { btnEliminar.Enabled = true; }
                    operario = true;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
            else
            { MessageBox.Show("Debe elegir un operario", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnEditarPaciente_Click(object sender, EventArgs e)
        {
            if (dgrPacientes.CurrentRow != null)
            {
                try
                {
                    txtApellido.Text = dgrPacientes.CurrentRow.Cells["Apellido"].Value.ToString();
                    txtNombre.Text = dgrPacientes.CurrentRow.Cells["Nombre"].Value.ToString();
                    txtDNI.Text = dgrPacientes.CurrentRow.Cells["DNI"].Value.ToString();
                    txtDomicilio.Text = dgrPacientes.CurrentRow.Cells["Domicilio"].Value.ToString();
                    txtTelefono.Text = dgrPacientes.CurrentRow.Cells["Telefono"].Value.ToString();
                    txtObservaciones.Text = dgrPacientes.CurrentRow.Cells["Observaciones"].Value.ToString();
                    dteFecha_Nacimiento.Value = DateTime.Parse(dgrPacientes.CurrentRow.Cells["Fecha_Nacimiento"].Value.ToString());
                    txtID.Text = dgrPacientes.CurrentRow.Cells["ID_Paciente"].Value.ToString();
                    chkActividad.Checked = (Boolean)dgrPacientes.CurrentRow.Cells["Actividad"].Value;

                    txtApellido.Enabled = true;
                    txtNombre.Enabled = true;
                    txtDNI.Enabled = true;
                    txtDomicilio.Enabled = true;
                    txtTelefono.Enabled = true;
                    txtObservaciones.Enabled = true;
                    dteFecha_Nacimiento.Enabled = true;
                    btnEditarMedico.Enabled = false;
                    btnEditarOperarios.Enabled = false;
                    btnEditarPaciente.Enabled = false;
                    btnGuardarCambios.Enabled = true;
                    btnCancelar.Enabled = true;
                    if (chkActividad.Checked == true)
                    { btnEliminar.Enabled = true; }
                    paciente = true;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
            else
            { MessageBox.Show("Debe elegir un paciente", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnEditarContraseña_Click(object sender, EventArgs e)
        {
            txtContraseña.Visible = true;
        }

        private void btnGuardarCambios_Click(object sender, EventArgs e)
        {
            if (medico)
            {
                if (txtContraseña.Visible == false)
                {
                    try
                    {
                        if (desencriptada)
                        {
                            MessageBox.Show(CRUD.editarMedico(txtID.Text, txtApellido.Text, txtNombre.Text, txtDNI.Text, HashPass.RSAEncrypt(txtNroMatricula.Text, false), dteFecha_Nacimiento.Value, txtDomicilio.Text,
                                txtTelefono.Text, txtObservaciones.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            MessageBox.Show(CRUD.editarMedico(txtID.Text, txtApellido.Text, txtNombre.Text, txtDNI.Text, txtNroMatricula.Text, dteFecha_Nacimiento.Value, txtDomicilio.Text,
                            txtTelefono.Text, txtObservaciones.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        EstadoInicial(); 
                        ActualizarDataGrids();
                    }
                    catch (DNIEnUso ex)
                    { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    catch (Exception ex)
                    { MessageBox.Show(ex.Message); }
                }
                else
                {
                    try
                    {
                        if (desencriptada)
                        {
                            MessageBox.Show(CRUD.editarMedico(txtID.Text, txtApellido.Text, txtNombre.Text, txtDNI.Text, HashPass.RSAEncrypt(txtNroMatricula.Text, false), dteFecha_Nacimiento.Value, txtDomicilio.Text,
                                txtTelefono.Text, txtObservaciones.Text, txtContraseña.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            MessageBox.Show(CRUD.editarMedico(txtID.Text, txtApellido.Text, txtNombre.Text, txtDNI.Text, txtNroMatricula.Text, dteFecha_Nacimiento.Value, txtDomicilio.Text,
                                txtTelefono.Text, txtObservaciones.Text, txtContraseña.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        EstadoInicial();
                        ActualizarDataGrids();
                    }
                    catch (DNIEnUso ex)
                    { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    catch (Exception ex)
                    { MessageBox.Show(ex.Message); }
                }
            }
            if (operario)
            {
                if (txtContraseña.Visible == false)
                {
                    try
                    {
                        MessageBox.Show(CRUD.editarOperario(txtID.Text, txtApellido.Text, txtNombre.Text, txtDNI.Text, dteFecha_Nacimiento.Value, txtDomicilio.Text,
                                txtTelefono.Text, txtObservaciones.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        EstadoInicial();
                        ActualizarDataGrids();
                    }
                    catch (DNIEnUso ex)
                    { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    catch (Exception ex)
                    { MessageBox.Show(ex.Message); }
                }
                else
                {
                    try
                    {
                        MessageBox.Show(CRUD.editarOperario(txtID.Text, txtApellido.Text, txtNombre.Text, txtDNI.Text, dteFecha_Nacimiento.Value, txtDomicilio.Text,
                                txtTelefono.Text, txtObservaciones.Text, txtContraseña.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        EstadoInicial();
                        ActualizarDataGrids();
                    }
                    catch (DNIEnUso ex)
                    { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    catch (Exception ex)
                    { MessageBox.Show(ex.Message); }
                }
            }
            if (paciente)
            {
                try
                {
                    MessageBox.Show(CRUD.editarPaciente(txtID.Text, txtApellido.Text, txtNombre.Text, txtDNI.Text, dteFecha_Nacimiento.Value, txtDomicilio.Text,
                                txtTelefono.Text, txtObservaciones.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    EstadoInicial();
                    ActualizarDataGrids();
                }
                catch (DNIEnUso ex)
                { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }

        private void EstadoInicial()
        {
            txtApellido.Text = String.Empty;
            txtNombre.Text = String.Empty;
            txtDNI.Text = String.Empty;
            txtDomicilio.Text = String.Empty;
            txtTelefono.Text = String.Empty;
            dteFecha_Nacimiento.Value = DateTime.Today;
            txtObservaciones.Text = String.Empty;
            txtNroMatricula.Text = String.Empty;
            txtID.Text = String.Empty;
            chkActividad.Checked = false;

            medico = false; operario = false; paciente = false; desencriptada = false;
            txtApellido.Enabled = false;
            txtNombre.Enabled = false;
            txtDNI.Enabled = false;
            txtDomicilio.Enabled = false;
            txtTelefono.Enabled = false;
            txtObservaciones.Enabled = false;
            dteFecha_Nacimiento.Enabled = false;
            txtNroMatricula.Enabled = false;
            btnGuardarCambios.Enabled = false;
            btnCancelar.Enabled = false;
            btnEditarContraseña.Enabled = false;
            txtContraseña.Visible = false;
            btnEditarMedico.Enabled = true;
            btnEditarOperarios.Enabled = true;
            btnEditarPaciente.Enabled = true;
            btnEliminar.Enabled = false;
            btnDesencriptar.Enabled = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            EstadoInicial();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (medico)
            {
                MessageBox.Show(CRUD.eliminarMedico(txtID.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            if (operario)
            {
                MessageBox.Show(CRUD.eliminarOperario(txtID.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            if (paciente)
            {
                MessageBox.Show(CRUD.eliminarPaciente(txtID.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            EstadoInicial();
            ActualizarDataGrids();
        }

        private void btnDesencriptar_Click(object sender, EventArgs e)
        {
            desencriptada = true;
            string matriculaVieja = txtNroMatricula.Text;
            try
            {
                txtNroMatricula.Text = HashPass.RSADecrypt(matriculaVieja, false);
            }
            catch (CryptographicException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {
            ActualizarDataGridsHistorial();
            EstadoInicial();
        }

    }
}
