﻿namespace Sistema_Clinica
{
    partial class LoginSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPageRegistro = new System.Windows.Forms.TabPage();
            this.lstTipoUsuarioRegistro = new System.Windows.Forms.ListBox();
            this.lblNroMatricula = new System.Windows.Forms.Label();
            this.txtNroMatricula = new System.Windows.Forms.TextBox();
            this.lblObservaciones = new System.Windows.Forms.Label();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.dteFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblApellido = new System.Windows.Forms.Label();
            this.lblDNI = new System.Windows.Forms.Label();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.lblDomicilio = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtDNI = new System.Windows.Forms.TextBox();
            this.txtDomicilio = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtRegisterConfirm = new System.Windows.Forms.TextBox();
            this.txtRegisterPassword = new System.Windows.Forms.TextBox();
            this.lblConfirmContraseña = new System.Windows.Forms.Label();
            this.lblRegisterContraseña = new System.Windows.Forms.Label();
            this.btnRegister = new System.Windows.Forms.Button();
            this.tabPageLogin = new System.Windows.Forms.TabPage();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lstTipoUsuarioLogin = new System.Windows.Forms.ListBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtLoginPassword = new System.Windows.Forms.TextBox();
            this.txtLoginUsername = new System.Windows.Forms.TextBox();
            this.lblLoginPwd = new System.Windows.Forms.Label();
            this.lblLoginUser = new System.Windows.Forms.Label();
            this.tabLoginSystem = new System.Windows.Forms.TabControl();
            this.tabPageAdministracion = new System.Windows.Forms.TabPage();
            this.btnEstadísticas = new System.Windows.Forms.Button();
            this.btnAdmin = new System.Windows.Forms.Button();
            this.btnSalir2 = new System.Windows.Forms.Button();
            this.tabPageRegistro.SuspendLayout();
            this.tabPageLogin.SuspendLayout();
            this.tabLoginSystem.SuspendLayout();
            this.tabPageAdministracion.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPageRegistro
            // 
            this.tabPageRegistro.Controls.Add(this.lstTipoUsuarioRegistro);
            this.tabPageRegistro.Controls.Add(this.lblNroMatricula);
            this.tabPageRegistro.Controls.Add(this.txtNroMatricula);
            this.tabPageRegistro.Controls.Add(this.lblObservaciones);
            this.tabPageRegistro.Controls.Add(this.txtObservaciones);
            this.tabPageRegistro.Controls.Add(this.dteFechaNacimiento);
            this.tabPageRegistro.Controls.Add(this.lblNombre);
            this.tabPageRegistro.Controls.Add(this.lblApellido);
            this.tabPageRegistro.Controls.Add(this.lblDNI);
            this.tabPageRegistro.Controls.Add(this.lblFechaNacimiento);
            this.tabPageRegistro.Controls.Add(this.lblDomicilio);
            this.tabPageRegistro.Controls.Add(this.lblTelefono);
            this.tabPageRegistro.Controls.Add(this.txtNombre);
            this.tabPageRegistro.Controls.Add(this.txtApellido);
            this.tabPageRegistro.Controls.Add(this.txtDNI);
            this.tabPageRegistro.Controls.Add(this.txtDomicilio);
            this.tabPageRegistro.Controls.Add(this.txtTelefono);
            this.tabPageRegistro.Controls.Add(this.txtRegisterConfirm);
            this.tabPageRegistro.Controls.Add(this.txtRegisterPassword);
            this.tabPageRegistro.Controls.Add(this.lblConfirmContraseña);
            this.tabPageRegistro.Controls.Add(this.lblRegisterContraseña);
            this.tabPageRegistro.Controls.Add(this.btnRegister);
            this.tabPageRegistro.Location = new System.Drawing.Point(4, 22);
            this.tabPageRegistro.Name = "tabPageRegistro";
            this.tabPageRegistro.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRegistro.Size = new System.Drawing.Size(476, 170);
            this.tabPageRegistro.TabIndex = 1;
            this.tabPageRegistro.Text = "Registro";
            this.tabPageRegistro.UseVisualStyleBackColor = true;
            // 
            // lstTipoUsuarioRegistro
            // 
            this.lstTipoUsuarioRegistro.FormattingEnabled = true;
            this.lstTipoUsuarioRegistro.Items.AddRange(new object[] {
            "Medico",
            "Operario"});
            this.lstTipoUsuarioRegistro.Location = new System.Drawing.Point(8, 126);
            this.lstTipoUsuarioRegistro.Name = "lstTipoUsuarioRegistro";
            this.lstTipoUsuarioRegistro.Size = new System.Drawing.Size(120, 30);
            this.lstTipoUsuarioRegistro.TabIndex = 10;
            this.lstTipoUsuarioRegistro.SelectedIndexChanged += new System.EventHandler(this.lstTipoUsuarioRegistro_SelectedIndexChanged);
            // 
            // lblNroMatricula
            // 
            this.lblNroMatricula.AutoSize = true;
            this.lblNroMatricula.Location = new System.Drawing.Point(176, 120);
            this.lblNroMatricula.Name = "lblNroMatricula";
            this.lblNroMatricula.Size = new System.Drawing.Size(65, 13);
            this.lblNroMatricula.TabIndex = 25;
            this.lblNroMatricula.Text = "Nº Matricula";
            // 
            // txtNroMatricula
            // 
            this.txtNroMatricula.Enabled = false;
            this.txtNroMatricula.Location = new System.Drawing.Point(177, 136);
            this.txtNroMatricula.Name = "txtNroMatricula";
            this.txtNroMatricula.Size = new System.Drawing.Size(120, 20);
            this.txtNroMatricula.TabIndex = 11;
            // 
            // lblObservaciones
            // 
            this.lblObservaciones.AutoSize = true;
            this.lblObservaciones.Location = new System.Drawing.Point(344, 81);
            this.lblObservaciones.Name = "lblObservaciones";
            this.lblObservaciones.Size = new System.Drawing.Size(78, 13);
            this.lblObservaciones.TabIndex = 23;
            this.lblObservaciones.Text = "Observaciones";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Location = new System.Drawing.Point(347, 97);
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(120, 20);
            this.txtObservaciones.TabIndex = 9;
            // 
            // dteFechaNacimiento
            // 
            this.dteFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dteFechaNacimiento.Location = new System.Drawing.Point(347, 58);
            this.dteFechaNacimiento.Name = "dteFechaNacimiento";
            this.dteFechaNacimiento.Size = new System.Drawing.Size(120, 20);
            this.dteFechaNacimiento.TabIndex = 6;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(175, 3);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 19;
            this.lblNombre.Text = "Nombre";
            // 
            // lblApellido
            // 
            this.lblApellido.AutoSize = true;
            this.lblApellido.Location = new System.Drawing.Point(6, 3);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(44, 13);
            this.lblApellido.TabIndex = 18;
            this.lblApellido.Text = "Apellido";
            // 
            // lblDNI
            // 
            this.lblDNI.AutoSize = true;
            this.lblDNI.Location = new System.Drawing.Point(344, 3);
            this.lblDNI.Name = "lblDNI";
            this.lblDNI.Size = new System.Drawing.Size(26, 13);
            this.lblDNI.TabIndex = 17;
            this.lblDNI.Text = "DNI";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(344, 42);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(108, 13);
            this.lblFechaNacimiento.TabIndex = 16;
            this.lblFechaNacimiento.Text = "Fecha de Nacimiento";
            // 
            // lblDomicilio
            // 
            this.lblDomicilio.AutoSize = true;
            this.lblDomicilio.Location = new System.Drawing.Point(175, 42);
            this.lblDomicilio.Name = "lblDomicilio";
            this.lblDomicilio.Size = new System.Drawing.Size(49, 13);
            this.lblDomicilio.TabIndex = 15;
            this.lblDomicilio.Text = "Domicilio";
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Location = new System.Drawing.Point(5, 42);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(49, 13);
            this.lblTelefono.TabIndex = 14;
            this.lblTelefono.Text = "Telefono";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(178, 19);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(120, 20);
            this.txtNombre.TabIndex = 2;
            this.txtNombre.Validating += new System.ComponentModel.CancelEventHandler(this.txtNombre_Validating);
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(8, 19);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(120, 20);
            this.txtApellido.TabIndex = 1;
            this.txtApellido.Validating += new System.ComponentModel.CancelEventHandler(this.txtApellido_Validating);
            // 
            // txtDNI
            // 
            this.txtDNI.Location = new System.Drawing.Point(347, 19);
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.Size = new System.Drawing.Size(120, 20);
            this.txtDNI.TabIndex = 3;
            this.txtDNI.Validating += new System.ComponentModel.CancelEventHandler(this.txtDNI_Validating);
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.Location = new System.Drawing.Point(179, 58);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.Size = new System.Drawing.Size(120, 20);
            this.txtDomicilio.TabIndex = 5;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(8, 58);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(120, 20);
            this.txtTelefono.TabIndex = 4;
            // 
            // txtRegisterConfirm
            // 
            this.txtRegisterConfirm.Location = new System.Drawing.Point(179, 97);
            this.txtRegisterConfirm.Name = "txtRegisterConfirm";
            this.txtRegisterConfirm.PasswordChar = '*';
            this.txtRegisterConfirm.Size = new System.Drawing.Size(120, 20);
            this.txtRegisterConfirm.TabIndex = 8;
            // 
            // txtRegisterPassword
            // 
            this.txtRegisterPassword.Location = new System.Drawing.Point(8, 97);
            this.txtRegisterPassword.Name = "txtRegisterPassword";
            this.txtRegisterPassword.PasswordChar = '*';
            this.txtRegisterPassword.Size = new System.Drawing.Size(120, 20);
            this.txtRegisterPassword.TabIndex = 7;
            // 
            // lblConfirmContraseña
            // 
            this.lblConfirmContraseña.AutoSize = true;
            this.lblConfirmContraseña.Location = new System.Drawing.Point(175, 81);
            this.lblConfirmContraseña.Name = "lblConfirmContraseña";
            this.lblConfirmContraseña.Size = new System.Drawing.Size(108, 13);
            this.lblConfirmContraseña.TabIndex = 3;
            this.lblConfirmContraseña.Text = "Confirmar Contraseña";
            // 
            // lblRegisterContraseña
            // 
            this.lblRegisterContraseña.AutoSize = true;
            this.lblRegisterContraseña.Location = new System.Drawing.Point(6, 81);
            this.lblRegisterContraseña.Name = "lblRegisterContraseña";
            this.lblRegisterContraseña.Size = new System.Drawing.Size(61, 13);
            this.lblRegisterContraseña.TabIndex = 2;
            this.lblRegisterContraseña.Text = "Contraseña";
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(347, 130);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(120, 30);
            this.btnRegister.TabIndex = 12;
            this.btnRegister.Text = "Registrar";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // tabPageLogin
            // 
            this.tabPageLogin.Controls.Add(this.btnSalir);
            this.tabPageLogin.Controls.Add(this.lstTipoUsuarioLogin);
            this.tabPageLogin.Controls.Add(this.btnLogin);
            this.tabPageLogin.Controls.Add(this.txtLoginPassword);
            this.tabPageLogin.Controls.Add(this.txtLoginUsername);
            this.tabPageLogin.Controls.Add(this.lblLoginPwd);
            this.tabPageLogin.Controls.Add(this.lblLoginUser);
            this.tabPageLogin.Location = new System.Drawing.Point(4, 22);
            this.tabPageLogin.Name = "tabPageLogin";
            this.tabPageLogin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLogin.Size = new System.Drawing.Size(476, 170);
            this.tabPageLogin.TabIndex = 0;
            this.tabPageLogin.Text = "Login";
            this.tabPageLogin.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(346, 126);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(122, 30);
            this.btnSalir.TabIndex = 5;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lstTipoUsuarioLogin
            // 
            this.lstTipoUsuarioLogin.FormattingEnabled = true;
            this.lstTipoUsuarioLogin.Items.AddRange(new object[] {
            "Medico",
            "Operario"});
            this.lstTipoUsuarioLogin.Location = new System.Drawing.Point(10, 90);
            this.lstTipoUsuarioLogin.Name = "lstTipoUsuarioLogin";
            this.lstTipoUsuarioLogin.Size = new System.Drawing.Size(120, 30);
            this.lstTipoUsuarioLogin.TabIndex = 3;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(10, 126);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(122, 30);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Ingresar";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtLoginPassword
            // 
            this.txtLoginPassword.Location = new System.Drawing.Point(8, 62);
            this.txtLoginPassword.Name = "txtLoginPassword";
            this.txtLoginPassword.PasswordChar = '*';
            this.txtLoginPassword.Size = new System.Drawing.Size(121, 20);
            this.txtLoginPassword.TabIndex = 2;
            // 
            // txtLoginUsername
            // 
            this.txtLoginUsername.Location = new System.Drawing.Point(8, 23);
            this.txtLoginUsername.Name = "txtLoginUsername";
            this.txtLoginUsername.Size = new System.Drawing.Size(121, 20);
            this.txtLoginUsername.TabIndex = 1;
            // 
            // lblLoginPwd
            // 
            this.lblLoginPwd.AutoSize = true;
            this.lblLoginPwd.Location = new System.Drawing.Point(6, 46);
            this.lblLoginPwd.Name = "lblLoginPwd";
            this.lblLoginPwd.Size = new System.Drawing.Size(61, 13);
            this.lblLoginPwd.TabIndex = 1;
            this.lblLoginPwd.Text = "Contraseña";
            // 
            // lblLoginUser
            // 
            this.lblLoginUser.AutoSize = true;
            this.lblLoginUser.Location = new System.Drawing.Point(5, 7);
            this.lblLoginUser.Name = "lblLoginUser";
            this.lblLoginUser.Size = new System.Drawing.Size(43, 13);
            this.lblLoginUser.TabIndex = 0;
            this.lblLoginUser.Text = "Usuario";
            // 
            // tabLoginSystem
            // 
            this.tabLoginSystem.Controls.Add(this.tabPageLogin);
            this.tabLoginSystem.Controls.Add(this.tabPageRegistro);
            this.tabLoginSystem.Controls.Add(this.tabPageAdministracion);
            this.tabLoginSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLoginSystem.Location = new System.Drawing.Point(0, 0);
            this.tabLoginSystem.Name = "tabLoginSystem";
            this.tabLoginSystem.SelectedIndex = 0;
            this.tabLoginSystem.Size = new System.Drawing.Size(484, 196);
            this.tabLoginSystem.TabIndex = 0;
            // 
            // tabPageAdministracion
            // 
            this.tabPageAdministracion.Controls.Add(this.btnEstadísticas);
            this.tabPageAdministracion.Controls.Add(this.btnAdmin);
            this.tabPageAdministracion.Controls.Add(this.btnSalir2);
            this.tabPageAdministracion.Location = new System.Drawing.Point(4, 22);
            this.tabPageAdministracion.Name = "tabPageAdministracion";
            this.tabPageAdministracion.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdministracion.Size = new System.Drawing.Size(476, 170);
            this.tabPageAdministracion.TabIndex = 2;
            this.tabPageAdministracion.Text = "Administracion";
            this.tabPageAdministracion.UseVisualStyleBackColor = true;
            // 
            // btnEstadísticas
            // 
            this.btnEstadísticas.Location = new System.Drawing.Point(8, 42);
            this.btnEstadísticas.Name = "btnEstadísticas";
            this.btnEstadísticas.Size = new System.Drawing.Size(121, 30);
            this.btnEstadísticas.TabIndex = 8;
            this.btnEstadísticas.Text = "Estadísticas";
            this.btnEstadísticas.UseVisualStyleBackColor = true;
            this.btnEstadísticas.Click += new System.EventHandler(this.btnEstadísticas_Click);
            // 
            // btnAdmin
            // 
            this.btnAdmin.Location = new System.Drawing.Point(8, 6);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(121, 30);
            this.btnAdmin.TabIndex = 7;
            this.btnAdmin.Text = "Administracion";
            this.btnAdmin.UseVisualStyleBackColor = true;
            this.btnAdmin.Click += new System.EventHandler(this.btnEditarRegistros_Click);
            // 
            // btnSalir2
            // 
            this.btnSalir2.Location = new System.Drawing.Point(346, 132);
            this.btnSalir2.Name = "btnSalir2";
            this.btnSalir2.Size = new System.Drawing.Size(122, 30);
            this.btnSalir2.TabIndex = 6;
            this.btnSalir2.Text = "Salir";
            this.btnSalir2.UseVisualStyleBackColor = true;
            this.btnSalir2.Click += new System.EventHandler(this.button1_Click);
            // 
            // LoginSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 196);
            this.Controls.Add(this.tabLoginSystem);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 235);
            this.MinimumSize = new System.Drawing.Size(500, 235);
            this.Name = "LoginSystem";
            this.Text = "Bienvenido";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginSystem_FormClosing);
            this.Load += new System.EventHandler(this.LoginSystem_Load);
            this.tabPageRegistro.ResumeLayout(false);
            this.tabPageRegistro.PerformLayout();
            this.tabPageLogin.ResumeLayout(false);
            this.tabPageLogin.PerformLayout();
            this.tabLoginSystem.ResumeLayout(false);
            this.tabPageAdministracion.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPageRegistro;
        private System.Windows.Forms.DateTimePicker dteFechaNacimiento;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblDNI;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.Label lblDomicilio;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtDNI;
        private System.Windows.Forms.TextBox txtDomicilio;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtRegisterConfirm;
        private System.Windows.Forms.TextBox txtRegisterPassword;
        private System.Windows.Forms.Label lblConfirmContraseña;
        private System.Windows.Forms.Label lblRegisterContraseña;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.TabPage tabPageLogin;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtLoginPassword;
        private System.Windows.Forms.TextBox txtLoginUsername;
        private System.Windows.Forms.Label lblLoginPwd;
        private System.Windows.Forms.Label lblLoginUser;
        private System.Windows.Forms.TabControl tabLoginSystem;
        private System.Windows.Forms.Label lblObservaciones;
        private System.Windows.Forms.Label lblNroMatricula;
        private System.Windows.Forms.TextBox txtNroMatricula;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.ListBox lstTipoUsuarioLogin;
        private System.Windows.Forms.ListBox lstTipoUsuarioRegistro;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.TabPage tabPageAdministracion;
        private System.Windows.Forms.Button btnSalir2;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Button btnEstadísticas;
    }
}

