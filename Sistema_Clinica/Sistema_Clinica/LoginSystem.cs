﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Clinica
{
    public partial class LoginSystem : Form
    {
        FormularioMedicos formularioMedicos;
        FormularioOperarios formularioOperarios;
        public LoginSystem()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            //Registramos el usuario según el rol
            try
            {
                    switch (lstTipoUsuarioRegistro.SelectedIndex)
                    {
                        case 0: //0 es medico
                            {
                                MessageBox.Show(CRUD.nuevoMedico(txtApellido.Text, txtNombre.Text, txtDNI.Text, txtDomicilio.Text, txtTelefono.Text, dteFechaNacimiento.Value, txtObservaciones.Text, txtNroMatricula.Text, txtRegisterPassword.Text, txtRegisterConfirm.Text),
                                    "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                //y vaciamos las textbox
                                txtRegisterPassword.Text = String.Empty;
                                txtRegisterConfirm.Text = String.Empty;
                                txtNombre.Text = String.Empty;
                                txtApellido.Text = String.Empty;
                                txtDNI.Text = String.Empty;
                                txtDomicilio.Text = String.Empty;
                                txtTelefono.Text = String.Empty;
                                txtObservaciones.Text = String.Empty;
                                txtNroMatricula.Text = String.Empty;
                                lstTipoUsuarioRegistro.SelectedIndex = -1;
                                dteFechaNacimiento.Value = DateTime.Today;
                                break;
                            }
                        case 1: // 1 es operario
                            {
                                MessageBox.Show(CRUD.nuevoOperario(txtApellido.Text, txtNombre.Text, txtDNI.Text, txtDomicilio.Text, txtTelefono.Text, dteFechaNacimiento.Value, txtObservaciones.Text, txtRegisterPassword.Text, txtRegisterConfirm.Text),
                                    "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                //y vaciamos las textbox
                                txtRegisterPassword.Text = String.Empty;
                                txtRegisterConfirm.Text = String.Empty;
                                txtNombre.Text = String.Empty;
                                txtApellido.Text = String.Empty;
                                txtDNI.Text = String.Empty;
                                txtDomicilio.Text = String.Empty;
                                txtTelefono.Text = String.Empty;
                                txtObservaciones.Text = String.Empty;
                                txtNroMatricula.Text = String.Empty;
                                lstTipoUsuarioRegistro.SelectedIndex = -1;
                                dteFechaNacimiento.Value = DateTime.Today;
                                break;
                            }
                        default:
                            { MessageBox.Show("Debe elegir un tipo de usuario", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); break; }
                    }
                }
            //Atrapamos todas las excepciones que pueden surgir
            catch (ArgumentNullException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (UsuarioExistente ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (ContraseñaNoCoincide ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (ContraseñaNoRespetaFormato ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (DatosPersonalesVacios ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (FormatException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (CryptographicException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtLoginPassword.Text.Length < 8) { throw new ContraseñaNoRespetaFormato("La contraseña debe contener al menos 8 carácteres"); }
                else if (txtLoginUsername.Text.Length < 7) { throw new FormatException("El DNI debe contener al menos 7 digitos"); }
                else
                {
                    string contraseñaEncriptada = HashPass.encriptarContraseña(txtLoginPassword.Text);
                    MessageBox.Show(CRUD.login(txtLoginUsername.Text, contraseñaEncriptada, lstTipoUsuarioLogin.SelectedIndex), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Hide();
                    switch (lstTipoUsuarioLogin.SelectedIndex)
                    {
                        case 0:
                            formularioMedicos = new FormularioMedicos(txtLoginUsername.Text);
                            formularioMedicos.ShowDialog();
                            formularioMedicos.Dispose();
                            break;
                        case 1:
                            this.Hide();
                            formularioOperarios = new FormularioOperarios(txtLoginUsername.Text);
                            formularioOperarios.ShowDialog();
                            formularioOperarios.Dispose();
                            break;
                    }
                    txtLoginUsername.Text = String.Empty;
                    txtLoginPassword.Text = String.Empty;
                    lstTipoUsuarioLogin.SelectedIndex = -1;
                    this.Show();
                }
            }
            catch (ArgumentNullException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (LoginFail ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (ContraseñaNoRespetaFormato ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (FormatException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void lstTipoUsuarioRegistro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstTipoUsuarioRegistro.SelectedIndex != 0)
            {
                txtNroMatricula.Enabled = false;
            }
            else
            {
                txtNroMatricula.Enabled = true;
            }
        }

        private void LoginSystem_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            { e.Cancel = true; }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEditarRegistros_Click(object sender, EventArgs e)
        {
            try
            {
                FormularioAdministracion form = new FormularioAdministracion();
                this.Hide();
                form.ShowDialog();
                form.Dispose();
                this.Show();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnEstadísticas_Click(object sender, EventArgs e)
        {
            try
            {
                FormularioEstadisticas form = new FormularioEstadisticas();
                this.Hide();
                form.ShowDialog();
                form.Dispose();
                this.Show();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void LoginSystem_Load(object sender, EventArgs e)
        {
            this.Text = ProductName + " v" + ProductVersion;
        }

        private void txtApellido_Validating(object sender, CancelEventArgs e)
        {
            string regexNumeros = "[0-9]";
            if (txtApellido.Text == "")
            {
                lblApellido.ForeColor = Color.Red;
                lblApellido.Text = "Apellido - Campo vacío";
                btnRegister.Enabled = false;
            }
            else if (Regex.IsMatch(txtApellido.Text, regexNumeros, RegexOptions.Singleline))
            {
                lblApellido.ForeColor = Color.Red;
                lblApellido.Text = "Apellido - Contiene números";
                btnRegister.Enabled = false;
            }
            else
            {
                lblApellido.ForeColor = Color.Black;
                lblApellido.Text = "Apellido";
                btnRegister.Enabled = true;
            }
        }

        private void txtNombre_Validating(object sender, CancelEventArgs e)
        {
            string regexNumeros = "[0-9]";
            if (txtNombre.Text == "")
            {
                lblNombre.ForeColor = Color.Red;
                lblNombre.Text = "Nombre - Campo vacío";
                btnRegister.Enabled = false;
            }
            else if (Regex.IsMatch(txtNombre.Text, regexNumeros, RegexOptions.Singleline))
            {
                lblNombre.ForeColor = Color.Red;
                lblNombre.Text = "Nombre - Contiene números";
                btnRegister.Enabled = false;
            }
            else
            {
                lblNombre.ForeColor = Color.Black;
                lblNombre.Text = "Nombre";
                btnRegister.Enabled = true;
            }
        }

        private void txtDNI_Validating(object sender, CancelEventArgs e)
        {
            string regexNumeros = "[^0-9]";
            if (txtDNI.Text == "")
            {
                lblDNI.ForeColor = Color.Red;
                lblDNI.Text = "DNI - Campo vacío";
                btnRegister.Enabled = false;
            }
            else if (Regex.IsMatch(txtDNI.Text, regexNumeros, RegexOptions.Singleline))
            {
                lblDNI.ForeColor = Color.Red;
                lblDNI.Text = "DNI - Tiene letras/símbolos";
                btnRegister.Enabled = false;
            }
            else
            {
                lblDNI.ForeColor = Color.Black;
                lblDNI.Text = "DNI";
                btnRegister.Enabled = true;
            }
        }
    }
}
