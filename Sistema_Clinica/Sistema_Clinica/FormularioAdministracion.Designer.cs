﻿namespace Sistema_Clinica
{
    partial class FormularioAdministracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMedicos = new System.Windows.Forms.Label();
            this.lblOperarios = new System.Windows.Forms.Label();
            this.dgrMedicos = new System.Windows.Forms.DataGridView();
            this.dgrOperarios = new System.Windows.Forms.DataGridView();
            this.btnEditarMedico = new System.Windows.Forms.Button();
            this.btnEditarOperarios = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardarCambios = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtDNI = new System.Windows.Forms.TextBox();
            this.txtDomicilio = new System.Windows.Forms.TextBox();
            this.dteFecha_Nacimiento = new System.Windows.Forms.DateTimePicker();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtNroMatricula = new System.Windows.Forms.TextBox();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblApellido = new System.Windows.Forms.Label();
            this.lblDNI = new System.Windows.Forms.Label();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.lblDomicilio = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.lblNroMatricula = new System.Windows.Forms.Label();
            this.lblObservaciones = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.dgrPacientes = new System.Windows.Forms.DataGridView();
            this.lblPacientes = new System.Windows.Forms.Label();
            this.btnEditarPaciente = new System.Windows.Forms.Button();
            this.btnEditarContraseña = new System.Windows.Forms.Button();
            this.txtContraseña = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnDesencriptar = new System.Windows.Forms.Button();
            this.btnHistorial = new System.Windows.Forms.Button();
            this.chkActividad = new System.Windows.Forms.CheckBox();
            this.tblLayout = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.dgrMedicos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrOperarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPacientes)).BeginInit();
            this.tblLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMedicos
            // 
            this.lblMedicos.AutoSize = true;
            this.lblMedicos.Location = new System.Drawing.Point(3, 0);
            this.lblMedicos.Name = "lblMedicos";
            this.lblMedicos.Size = new System.Drawing.Size(47, 13);
            this.lblMedicos.TabIndex = 0;
            this.lblMedicos.Text = "Medicos";
            // 
            // lblOperarios
            // 
            this.lblOperarios.AutoSize = true;
            this.lblOperarios.Location = new System.Drawing.Point(309, 0);
            this.lblOperarios.Name = "lblOperarios";
            this.lblOperarios.Size = new System.Drawing.Size(52, 13);
            this.lblOperarios.TabIndex = 1;
            this.lblOperarios.Text = "Operarios";
            // 
            // dgrMedicos
            // 
            this.dgrMedicos.AllowUserToAddRows = false;
            this.dgrMedicos.AllowUserToDeleteRows = false;
            this.dgrMedicos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgrMedicos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgrMedicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrMedicos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrMedicos.Location = new System.Drawing.Point(3, 23);
            this.dgrMedicos.MultiSelect = false;
            this.dgrMedicos.Name = "dgrMedicos";
            this.dgrMedicos.ReadOnly = true;
            this.dgrMedicos.RowHeadersVisible = false;
            this.dgrMedicos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrMedicos.Size = new System.Drawing.Size(300, 228);
            this.dgrMedicos.TabIndex = 1;
            // 
            // dgrOperarios
            // 
            this.dgrOperarios.AllowUserToAddRows = false;
            this.dgrOperarios.AllowUserToDeleteRows = false;
            this.dgrOperarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgrOperarios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgrOperarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrOperarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrOperarios.Location = new System.Drawing.Point(309, 23);
            this.dgrOperarios.MultiSelect = false;
            this.dgrOperarios.Name = "dgrOperarios";
            this.dgrOperarios.ReadOnly = true;
            this.dgrOperarios.RowHeadersVisible = false;
            this.dgrOperarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrOperarios.Size = new System.Drawing.Size(300, 228);
            this.dgrOperarios.TabIndex = 2;
            // 
            // btnEditarMedico
            // 
            this.btnEditarMedico.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnEditarMedico.Location = new System.Drawing.Point(12, 311);
            this.btnEditarMedico.Name = "btnEditarMedico";
            this.btnEditarMedico.Size = new System.Drawing.Size(100, 23);
            this.btnEditarMedico.TabIndex = 4;
            this.btnEditarMedico.Text = "Editar Medico";
            this.btnEditarMedico.UseVisualStyleBackColor = true;
            this.btnEditarMedico.Click += new System.EventHandler(this.btnEditarMedico_Click);
            // 
            // btnEditarOperarios
            // 
            this.btnEditarOperarios.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnEditarOperarios.Location = new System.Drawing.Point(118, 311);
            this.btnEditarOperarios.Name = "btnEditarOperarios";
            this.btnEditarOperarios.Size = new System.Drawing.Size(100, 23);
            this.btnEditarOperarios.TabIndex = 5;
            this.btnEditarOperarios.Text = "Editar Operario";
            this.btnEditarOperarios.UseVisualStyleBackColor = true;
            this.btnEditarOperarios.Click += new System.EventHandler(this.btnEditarOperarios_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSalir.Location = new System.Drawing.Point(829, 311);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(100, 23);
            this.btnSalir.TabIndex = 20;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardarCambios
            // 
            this.btnGuardarCambios.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGuardarCambios.Enabled = false;
            this.btnGuardarCambios.Location = new System.Drawing.Point(617, 311);
            this.btnGuardarCambios.Name = "btnGuardarCambios";
            this.btnGuardarCambios.Size = new System.Drawing.Size(100, 23);
            this.btnGuardarCambios.TabIndex = 16;
            this.btnGuardarCambios.Text = "Guardar Cambios";
            this.btnGuardarCambios.UseVisualStyleBackColor = true;
            this.btnGuardarCambios.Click += new System.EventHandler(this.btnGuardarCambios_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(118, 285);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 8;
            // 
            // txtApellido
            // 
            this.txtApellido.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtApellido.Enabled = false;
            this.txtApellido.Location = new System.Drawing.Point(12, 285);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(100, 20);
            this.txtApellido.TabIndex = 7;
            // 
            // txtDNI
            // 
            this.txtDNI.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtDNI.Enabled = false;
            this.txtDNI.Location = new System.Drawing.Point(224, 285);
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.Size = new System.Drawing.Size(100, 20);
            this.txtDNI.TabIndex = 9;
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtDomicilio.Enabled = false;
            this.txtDomicilio.Location = new System.Drawing.Point(436, 285);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.Size = new System.Drawing.Size(100, 20);
            this.txtDomicilio.TabIndex = 11;
            // 
            // dteFecha_Nacimiento
            // 
            this.dteFecha_Nacimiento.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dteFecha_Nacimiento.Enabled = false;
            this.dteFecha_Nacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dteFecha_Nacimiento.Location = new System.Drawing.Point(330, 285);
            this.dteFecha_Nacimiento.Name = "dteFecha_Nacimiento";
            this.dteFecha_Nacimiento.Size = new System.Drawing.Size(100, 20);
            this.dteFecha_Nacimiento.TabIndex = 10;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtTelefono.Enabled = false;
            this.txtTelefono.Location = new System.Drawing.Point(542, 285);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(100, 20);
            this.txtTelefono.TabIndex = 12;
            // 
            // txtNroMatricula
            // 
            this.txtNroMatricula.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtNroMatricula.Enabled = false;
            this.txtNroMatricula.Location = new System.Drawing.Point(648, 285);
            this.txtNroMatricula.Name = "txtNroMatricula";
            this.txtNroMatricula.Size = new System.Drawing.Size(100, 20);
            this.txtNroMatricula.TabIndex = 13;
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtObservaciones.Enabled = false;
            this.txtObservaciones.Location = new System.Drawing.Point(754, 285);
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(100, 20);
            this.txtObservaciones.TabIndex = 14;
            // 
            // lblNombre
            // 
            this.lblNombre.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(115, 269);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 17;
            this.lblNombre.Text = "Nombre";
            // 
            // lblApellido
            // 
            this.lblApellido.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblApellido.AutoSize = true;
            this.lblApellido.Location = new System.Drawing.Point(9, 269);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(44, 13);
            this.lblApellido.TabIndex = 18;
            this.lblApellido.Text = "Apellido";
            // 
            // lblDNI
            // 
            this.lblDNI.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblDNI.AutoSize = true;
            this.lblDNI.Location = new System.Drawing.Point(224, 269);
            this.lblDNI.Name = "lblDNI";
            this.lblDNI.Size = new System.Drawing.Size(26, 13);
            this.lblDNI.TabIndex = 19;
            this.lblDNI.Text = "DNI";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(327, 269);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(93, 13);
            this.lblFechaNacimiento.TabIndex = 20;
            this.lblFechaNacimiento.Text = "Fecha Nacimiento";
            // 
            // lblDomicilio
            // 
            this.lblDomicilio.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblDomicilio.AutoSize = true;
            this.lblDomicilio.Location = new System.Drawing.Point(433, 269);
            this.lblDomicilio.Name = "lblDomicilio";
            this.lblDomicilio.Size = new System.Drawing.Size(49, 13);
            this.lblDomicilio.TabIndex = 21;
            this.lblDomicilio.Text = "Domicilio";
            // 
            // lblTelefono
            // 
            this.lblTelefono.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Location = new System.Drawing.Point(542, 269);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(49, 13);
            this.lblTelefono.TabIndex = 22;
            this.lblTelefono.Text = "Telefono";
            // 
            // lblNroMatricula
            // 
            this.lblNroMatricula.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblNroMatricula.AutoSize = true;
            this.lblNroMatricula.Location = new System.Drawing.Point(645, 269);
            this.lblNroMatricula.Name = "lblNroMatricula";
            this.lblNroMatricula.Size = new System.Drawing.Size(65, 13);
            this.lblNroMatricula.TabIndex = 23;
            this.lblNroMatricula.Text = "Nº Matricula";
            // 
            // lblObservaciones
            // 
            this.lblObservaciones.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblObservaciones.AutoSize = true;
            this.lblObservaciones.Location = new System.Drawing.Point(751, 269);
            this.lblObservaciones.Name = "lblObservaciones";
            this.lblObservaciones.Size = new System.Drawing.Size(78, 13);
            this.lblObservaciones.TabIndex = 24;
            this.lblObservaciones.Text = "Observaciones";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancelar.Enabled = false;
            this.btnCancelar.Location = new System.Drawing.Point(723, 311);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 23);
            this.btnCancelar.TabIndex = 19;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // dgrPacientes
            // 
            this.dgrPacientes.AllowUserToAddRows = false;
            this.dgrPacientes.AllowUserToDeleteRows = false;
            this.dgrPacientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgrPacientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgrPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrPacientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrPacientes.Location = new System.Drawing.Point(615, 23);
            this.dgrPacientes.MultiSelect = false;
            this.dgrPacientes.Name = "dgrPacientes";
            this.dgrPacientes.ReadOnly = true;
            this.dgrPacientes.RowHeadersVisible = false;
            this.dgrPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrPacientes.Size = new System.Drawing.Size(299, 228);
            this.dgrPacientes.TabIndex = 3;
            // 
            // lblPacientes
            // 
            this.lblPacientes.AutoSize = true;
            this.lblPacientes.Location = new System.Drawing.Point(615, 0);
            this.lblPacientes.Name = "lblPacientes";
            this.lblPacientes.Size = new System.Drawing.Size(54, 13);
            this.lblPacientes.TabIndex = 27;
            this.lblPacientes.Text = "Pacientes";
            // 
            // btnEditarPaciente
            // 
            this.btnEditarPaciente.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnEditarPaciente.Location = new System.Drawing.Point(224, 311);
            this.btnEditarPaciente.Name = "btnEditarPaciente";
            this.btnEditarPaciente.Size = new System.Drawing.Size(100, 23);
            this.btnEditarPaciente.TabIndex = 6;
            this.btnEditarPaciente.Text = "Editar Paciente";
            this.btnEditarPaciente.UseVisualStyleBackColor = true;
            this.btnEditarPaciente.Click += new System.EventHandler(this.btnEditarPaciente_Click);
            // 
            // btnEditarContraseña
            // 
            this.btnEditarContraseña.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnEditarContraseña.Enabled = false;
            this.btnEditarContraseña.Location = new System.Drawing.Point(12, 339);
            this.btnEditarContraseña.Name = "btnEditarContraseña";
            this.btnEditarContraseña.Size = new System.Drawing.Size(100, 23);
            this.btnEditarContraseña.TabIndex = 15;
            this.btnEditarContraseña.Text = "Editar Contraseña";
            this.btnEditarContraseña.UseVisualStyleBackColor = true;
            this.btnEditarContraseña.Click += new System.EventHandler(this.btnEditarContraseña_Click);
            // 
            // txtContraseña
            // 
            this.txtContraseña.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtContraseña.Location = new System.Drawing.Point(118, 342);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.Size = new System.Drawing.Size(100, 20);
            this.txtContraseña.TabIndex = 17;
            this.txtContraseña.Visible = false;
            // 
            // txtID
            // 
            this.txtID.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(860, 285);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(69, 20);
            this.txtID.TabIndex = 31;
            this.txtID.Visible = false;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnEliminar.Enabled = false;
            this.btnEliminar.Location = new System.Drawing.Point(511, 311);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(100, 23);
            this.btnEliminar.TabIndex = 18;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnDesencriptar
            // 
            this.btnDesencriptar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDesencriptar.Enabled = false;
            this.btnDesencriptar.Location = new System.Drawing.Point(330, 311);
            this.btnDesencriptar.Name = "btnDesencriptar";
            this.btnDesencriptar.Size = new System.Drawing.Size(100, 23);
            this.btnDesencriptar.TabIndex = 32;
            this.btnDesencriptar.Text = "Desencriptar";
            this.btnDesencriptar.UseVisualStyleBackColor = true;
            this.btnDesencriptar.Click += new System.EventHandler(this.btnDesencriptar_Click);
            // 
            // btnHistorial
            // 
            this.btnHistorial.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnHistorial.Location = new System.Drawing.Point(829, 339);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(100, 23);
            this.btnHistorial.TabIndex = 33;
            this.btnHistorial.Text = "Ver Historial";
            this.btnHistorial.UseVisualStyleBackColor = true;
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // chkActividad
            // 
            this.chkActividad.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.chkActividad.AutoSize = true;
            this.chkActividad.Location = new System.Drawing.Point(742, 345);
            this.chkActividad.Name = "chkActividad";
            this.chkActividad.Size = new System.Drawing.Size(70, 17);
            this.chkActividad.TabIndex = 34;
            this.chkActividad.Text = "Actividad";
            this.chkActividad.UseVisualStyleBackColor = true;
            this.chkActividad.Visible = false;
            // 
            // tblLayout
            // 
            this.tblLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tblLayout.ColumnCount = 3;
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.4502F));
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.43722F));
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.11258F));
            this.tblLayout.Controls.Add(this.dgrMedicos, 0, 1);
            this.tblLayout.Controls.Add(this.dgrPacientes, 2, 1);
            this.tblLayout.Controls.Add(this.dgrOperarios, 1, 1);
            this.tblLayout.Controls.Add(this.lblOperarios, 1, 0);
            this.tblLayout.Controls.Add(this.lblPacientes, 2, 0);
            this.tblLayout.Controls.Add(this.lblMedicos, 0, 0);
            this.tblLayout.Location = new System.Drawing.Point(12, 12);
            this.tblLayout.Name = "tblLayout";
            this.tblLayout.RowCount = 2;
            this.tblLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayout.Size = new System.Drawing.Size(917, 254);
            this.tblLayout.TabIndex = 35;
            // 
            // FormularioAdministracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 371);
            this.Controls.Add(this.tblLayout);
            this.Controls.Add(this.chkActividad);
            this.Controls.Add(this.btnHistorial);
            this.Controls.Add(this.btnDesencriptar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.txtContraseña);
            this.Controls.Add(this.btnEditarContraseña);
            this.Controls.Add(this.btnEditarPaciente);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblObservaciones);
            this.Controls.Add(this.lblNroMatricula);
            this.Controls.Add(this.lblTelefono);
            this.Controls.Add(this.lblDomicilio);
            this.Controls.Add(this.lblFechaNacimiento);
            this.Controls.Add(this.lblDNI);
            this.Controls.Add(this.lblApellido);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.txtObservaciones);
            this.Controls.Add(this.txtNroMatricula);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.dteFecha_Nacimiento);
            this.Controls.Add(this.txtDomicilio);
            this.Controls.Add(this.txtDNI);
            this.Controls.Add(this.txtApellido);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.btnGuardarCambios);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnEditarOperarios);
            this.Controls.Add(this.btnEditarMedico);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(952, 409);
            this.Name = "FormularioAdministracion";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Administración";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormularioEliminarRegistros_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgrMedicos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrOperarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPacientes)).EndInit();
            this.tblLayout.ResumeLayout(false);
            this.tblLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMedicos;
        private System.Windows.Forms.Label lblOperarios;
        private System.Windows.Forms.DataGridView dgrMedicos;
        private System.Windows.Forms.DataGridView dgrOperarios;
        private System.Windows.Forms.Button btnEditarMedico;
        private System.Windows.Forms.Button btnEditarOperarios;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnGuardarCambios;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtDNI;
        private System.Windows.Forms.TextBox txtDomicilio;
        private System.Windows.Forms.DateTimePicker dteFecha_Nacimiento;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtNroMatricula;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblDNI;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.Label lblDomicilio;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.Label lblNroMatricula;
        private System.Windows.Forms.Label lblObservaciones;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.DataGridView dgrPacientes;
        private System.Windows.Forms.Label lblPacientes;
        private System.Windows.Forms.Button btnEditarPaciente;
        private System.Windows.Forms.Button btnEditarContraseña;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnDesencriptar;
        private System.Windows.Forms.Button btnHistorial;
        private System.Windows.Forms.CheckBox chkActividad;
        private System.Windows.Forms.TableLayoutPanel tblLayout;
    }
}