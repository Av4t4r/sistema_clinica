﻿namespace Sistema_Clinica
{
    partial class FormularioMedicos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabFormMedicos = new System.Windows.Forms.TabControl();
            this.tabTurnos = new System.Windows.Forms.TabPage();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnHistorialTurnos = new System.Windows.Forms.Button();
            this.dgrTurnos = new System.Windows.Forms.DataGridView();
            this.mthCalendario = new System.Windows.Forms.MonthCalendar();
            this.tabHistorialTurnos = new System.Windows.Forms.TabPage();
            this.dgrPacientes = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabFormMedicos.SuspendLayout();
            this.tabTurnos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTurnos)).BeginInit();
            this.tabHistorialTurnos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPacientes)).BeginInit();
            this.SuspendLayout();
            // 
            // tabFormMedicos
            // 
            this.tabFormMedicos.Controls.Add(this.tabTurnos);
            this.tabFormMedicos.Controls.Add(this.tabHistorialTurnos);
            this.tabFormMedicos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormMedicos.Location = new System.Drawing.Point(0, 0);
            this.tabFormMedicos.Name = "tabFormMedicos";
            this.tabFormMedicos.SelectedIndex = 0;
            this.tabFormMedicos.Size = new System.Drawing.Size(1028, 408);
            this.tabFormMedicos.TabIndex = 0;
            // 
            // tabTurnos
            // 
            this.tabTurnos.Controls.Add(this.btnSalir);
            this.tabTurnos.Controls.Add(this.btnHistorialTurnos);
            this.tabTurnos.Controls.Add(this.dgrTurnos);
            this.tabTurnos.Controls.Add(this.mthCalendario);
            this.tabTurnos.Location = new System.Drawing.Point(4, 22);
            this.tabTurnos.Name = "tabTurnos";
            this.tabTurnos.Padding = new System.Windows.Forms.Padding(3);
            this.tabTurnos.Size = new System.Drawing.Size(1020, 382);
            this.tabTurnos.TabIndex = 0;
            this.tabTurnos.Text = "Turnos";
            this.tabTurnos.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSalir.Location = new System.Drawing.Point(8, 353);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(216, 23);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnHistorialTurnos
            // 
            this.btnHistorialTurnos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHistorialTurnos.Location = new System.Drawing.Point(8, 324);
            this.btnHistorialTurnos.Name = "btnHistorialTurnos";
            this.btnHistorialTurnos.Size = new System.Drawing.Size(216, 23);
            this.btnHistorialTurnos.TabIndex = 2;
            this.btnHistorialTurnos.Text = "Ver Historial Turnos";
            this.btnHistorialTurnos.UseVisualStyleBackColor = true;
            this.btnHistorialTurnos.Click += new System.EventHandler(this.btnTurnosAnteriores_Click);
            // 
            // dgrTurnos
            // 
            this.dgrTurnos.AllowUserToAddRows = false;
            this.dgrTurnos.AllowUserToDeleteRows = false;
            this.dgrTurnos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrTurnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrTurnos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrTurnos.Location = new System.Drawing.Point(230, 3);
            this.dgrTurnos.Name = "dgrTurnos";
            this.dgrTurnos.ReadOnly = true;
            this.dgrTurnos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrTurnos.Size = new System.Drawing.Size(787, 376);
            this.dgrTurnos.TabIndex = 1;
            // 
            // mthCalendario
            // 
            this.mthCalendario.CalendarDimensions = new System.Drawing.Size(1, 2);
            this.mthCalendario.Dock = System.Windows.Forms.DockStyle.Left;
            this.mthCalendario.Location = new System.Drawing.Point(3, 3);
            this.mthCalendario.MaxSelectionCount = 1;
            this.mthCalendario.Name = "mthCalendario";
            this.mthCalendario.TabIndex = 0;
            this.mthCalendario.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.mthCalendario_DateSelected);
            // 
            // tabHistorialTurnos
            // 
            this.tabHistorialTurnos.Controls.Add(this.dgrPacientes);
            this.tabHistorialTurnos.Location = new System.Drawing.Point(4, 22);
            this.tabHistorialTurnos.Name = "tabHistorialTurnos";
            this.tabHistorialTurnos.Padding = new System.Windows.Forms.Padding(3);
            this.tabHistorialTurnos.Size = new System.Drawing.Size(1020, 382);
            this.tabHistorialTurnos.TabIndex = 1;
            this.tabHistorialTurnos.Text = "Historial Turnos";
            this.tabHistorialTurnos.UseVisualStyleBackColor = true;
            // 
            // dgrPacientes
            // 
            this.dgrPacientes.AllowUserToAddRows = false;
            this.dgrPacientes.AllowUserToDeleteRows = false;
            this.dgrPacientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrPacientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrPacientes.Location = new System.Drawing.Point(3, 3);
            this.dgrPacientes.MultiSelect = false;
            this.dgrPacientes.Name = "dgrPacientes";
            this.dgrPacientes.ReadOnly = true;
            this.dgrPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrPacientes.Size = new System.Drawing.Size(1014, 376);
            this.dgrPacientes.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID_Paciente";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID_Paciente";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nombre";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Apellido";
            this.dataGridViewTextBoxColumn3.HeaderText = "Apellido";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DNI";
            this.dataGridViewTextBoxColumn4.HeaderText = "DNI";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Domicilio";
            this.dataGridViewTextBoxColumn5.HeaderText = "Domicilio";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn6.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Observaciones";
            this.dataGridViewTextBoxColumn7.HeaderText = "Observaciones";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ID_Operario";
            this.dataGridViewTextBoxColumn8.HeaderText = "ID_Operario";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // FormularioMedicos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 408);
            this.Controls.Add(this.tabFormMedicos);
            this.MinimumSize = new System.Drawing.Size(1022, 435);
            this.Name = "FormularioMedicos";
            this.ShowIcon = false;
            this.Text = "Medicos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormularioMedicos_FormClosing);
            this.Load += new System.EventHandler(this.FormularioMedicos_Load);
            this.tabFormMedicos.ResumeLayout(false);
            this.tabTurnos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrTurnos)).EndInit();
            this.tabHistorialTurnos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrPacientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabFormMedicos;
        private System.Windows.Forms.TabPage tabTurnos;
        private System.Windows.Forms.TabPage tabHistorialTurnos;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.MonthCalendar mthCalendario;
        private System.Windows.Forms.DataGridView dgrPacientes;
        private System.Windows.Forms.DataGridView dgrTurnos;
        private System.Windows.Forms.Button btnHistorialTurnos;
        private System.Windows.Forms.Button btnSalir;

    }
}