﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Clinica
{
    public partial class FormularioTurnoEditar : Form
    {
        string ID_Turno;

        public FormularioTurnoEditar(string ID_Turno, DateTime fecha, string horario, string motivo_consulta, string observaciones, bool actividad, bool asistencia)
        {
            InitializeComponent();
            this.ID_Turno = ID_Turno;
            mthFecha.SetDate(fecha);
            txtHorario.Text = horario;
            txtMotivo.Text = motivo_consulta;
            txtObservaciones.Text = observaciones;
            chkActividad.Checked = actividad;
            chkAsistencia.Enabled = true;
            chkAsistencia.Checked = asistencia;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(CRUD.editarTurno(ID_Turno, mthFecha.SelectionStart, txtHorario.Text, txtMotivo.Text, txtObservaciones.Text, chkActividad.Checked, chkAsistencia.Checked), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.FormClosing -= FormularioTurnoEditar_FormClosing; //Desuscribimos el handler del evento FormClosing asi no pregunta si queremos salir
                this.Close();
            }
            catch (FechaFueraDeRango ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (ArgumentNullException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (FormatException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (TurnoNoExistente ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtHorario_TextChanged(object sender, EventArgs e)
        {
            if (txtHorario.Text != "" && txtMotivo.Text != "")
            {
                btnGuardar.Enabled = true;
            }
            else
            {
                btnGuardar.Enabled = false;
            }
        }

        private void txtMotivo_TextChanged(object sender, EventArgs e)
        {
            if (txtHorario.Text != "" && txtMotivo.Text != "")
            {
                btnGuardar.Enabled = true;
            }
            else
            {
                btnGuardar.Enabled = false;
            }
        }

        private void FormularioTurnoEditar_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea cancelar?", "Cancelar Acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            { e.Cancel = true; }
        }
    }
}
