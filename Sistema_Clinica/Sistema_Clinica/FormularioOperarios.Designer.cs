﻿namespace Sistema_Clinica
{
    partial class FormularioOperarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabFormOperarios = new System.Windows.Forms.TabControl();
            this.tabTurnos = new System.Windows.Forms.TabPage();
            this.btnLimpiarPacientes = new System.Windows.Forms.Button();
            this.btnBuscarPaciente = new System.Windows.Forms.Button();
            this.txtBusquedaPaciente = new System.Windows.Forms.TextBox();
            this.lblListaPacientes = new System.Windows.Forms.Label();
            this.dgrListaPacientes = new System.Windows.Forms.DataGridView();
            this.btnLimpiarMedicos = new System.Windows.Forms.Button();
            this.lblListaMedicos = new System.Windows.Forms.Label();
            this.dgrListaMedicos = new System.Windows.Forms.DataGridView();
            this.btnTurnoNuevo = new System.Windows.Forms.Button();
            this.btnCambiarTurno = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.dgrTurnosPorMedico = new System.Windows.Forms.DataGridView();
            this.btnBuscarMedico = new System.Windows.Forms.Button();
            this.txtBusquedaMedico = new System.Windows.Forms.TextBox();
            this.tabPacientes = new System.Windows.Forms.TabPage();
            this.btnCancelarCambios = new System.Windows.Forms.Button();
            this.btnGuardarCambios = new System.Windows.Forms.Button();
            this.btnSalir2 = new System.Windows.Forms.Button();
            this.btnEditarPaciente = new System.Windows.Forms.Button();
            this.btnNuevoPaciente = new System.Windows.Forms.Button();
            this.dgrPacientes = new System.Windows.Forms.DataGridView();
            this.lblObservaciones = new System.Windows.Forms.Label();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.dteFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblApellido = new System.Windows.Forms.Label();
            this.lblDNI = new System.Windows.Forms.Label();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.lblDomicilio = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtDNI = new System.Windows.Forms.TextBox();
            this.txtDomicilio = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.tabFormOperarios.SuspendLayout();
            this.tabTurnos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrListaPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrListaMedicos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTurnosPorMedico)).BeginInit();
            this.tabPacientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPacientes)).BeginInit();
            this.SuspendLayout();
            // 
            // tabFormOperarios
            // 
            this.tabFormOperarios.Controls.Add(this.tabTurnos);
            this.tabFormOperarios.Controls.Add(this.tabPacientes);
            this.tabFormOperarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormOperarios.Location = new System.Drawing.Point(0, 0);
            this.tabFormOperarios.Name = "tabFormOperarios";
            this.tabFormOperarios.SelectedIndex = 0;
            this.tabFormOperarios.Size = new System.Drawing.Size(1028, 493);
            this.tabFormOperarios.TabIndex = 0;
            // 
            // tabTurnos
            // 
            this.tabTurnos.Controls.Add(this.btnLimpiarPacientes);
            this.tabTurnos.Controls.Add(this.btnBuscarPaciente);
            this.tabTurnos.Controls.Add(this.txtBusquedaPaciente);
            this.tabTurnos.Controls.Add(this.lblListaPacientes);
            this.tabTurnos.Controls.Add(this.dgrListaPacientes);
            this.tabTurnos.Controls.Add(this.btnLimpiarMedicos);
            this.tabTurnos.Controls.Add(this.lblListaMedicos);
            this.tabTurnos.Controls.Add(this.dgrListaMedicos);
            this.tabTurnos.Controls.Add(this.btnTurnoNuevo);
            this.tabTurnos.Controls.Add(this.btnCambiarTurno);
            this.tabTurnos.Controls.Add(this.btnSalir);
            this.tabTurnos.Controls.Add(this.dgrTurnosPorMedico);
            this.tabTurnos.Controls.Add(this.btnBuscarMedico);
            this.tabTurnos.Controls.Add(this.txtBusquedaMedico);
            this.tabTurnos.Location = new System.Drawing.Point(4, 22);
            this.tabTurnos.Name = "tabTurnos";
            this.tabTurnos.Padding = new System.Windows.Forms.Padding(3);
            this.tabTurnos.Size = new System.Drawing.Size(1020, 467);
            this.tabTurnos.TabIndex = 0;
            this.tabTurnos.Text = "Turnos";
            this.tabTurnos.UseVisualStyleBackColor = true;
            // 
            // btnLimpiarPacientes
            // 
            this.btnLimpiarPacientes.Location = new System.Drawing.Point(338, 201);
            this.btnLimpiarPacientes.Name = "btnLimpiarPacientes";
            this.btnLimpiarPacientes.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiarPacientes.TabIndex = 7;
            this.btnLimpiarPacientes.Text = "Limpiar";
            this.btnLimpiarPacientes.UseVisualStyleBackColor = true;
            this.btnLimpiarPacientes.Click += new System.EventHandler(this.btnLimpiarPacientes_Click);
            // 
            // btnBuscarPaciente
            // 
            this.btnBuscarPaciente.Enabled = false;
            this.btnBuscarPaciente.Location = new System.Drawing.Point(257, 201);
            this.btnBuscarPaciente.Name = "btnBuscarPaciente";
            this.btnBuscarPaciente.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarPaciente.TabIndex = 6;
            this.btnBuscarPaciente.Text = "Buscar";
            this.btnBuscarPaciente.UseVisualStyleBackColor = true;
            this.btnBuscarPaciente.Click += new System.EventHandler(this.btnBuscarPaciente_Click);
            // 
            // txtBusquedaPaciente
            // 
            this.txtBusquedaPaciente.Location = new System.Drawing.Point(11, 204);
            this.txtBusquedaPaciente.Name = "txtBusquedaPaciente";
            this.txtBusquedaPaciente.Size = new System.Drawing.Size(243, 20);
            this.txtBusquedaPaciente.TabIndex = 5;
            this.txtBusquedaPaciente.TextChanged += new System.EventHandler(this.txtBusquedaPaciente_TextChanged);
            // 
            // lblListaPacientes
            // 
            this.lblListaPacientes.AutoSize = true;
            this.lblListaPacientes.Location = new System.Drawing.Point(8, 227);
            this.lblListaPacientes.Name = "lblListaPacientes";
            this.lblListaPacientes.Size = new System.Drawing.Size(105, 13);
            this.lblListaPacientes.TabIndex = 11;
            this.lblListaPacientes.Text = "Listado de pacientes";
            // 
            // dgrListaPacientes
            // 
            this.dgrListaPacientes.AllowUserToAddRows = false;
            this.dgrListaPacientes.AllowUserToDeleteRows = false;
            this.dgrListaPacientes.AllowUserToResizeRows = false;
            this.dgrListaPacientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgrListaPacientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrListaPacientes.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgrListaPacientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dgrListaPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrListaPacientes.Location = new System.Drawing.Point(8, 243);
            this.dgrListaPacientes.MultiSelect = false;
            this.dgrListaPacientes.Name = "dgrListaPacientes";
            this.dgrListaPacientes.ReadOnly = true;
            this.dgrListaPacientes.RowHeadersWidth = 25;
            this.dgrListaPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrListaPacientes.Size = new System.Drawing.Size(405, 158);
            this.dgrListaPacientes.StandardTab = true;
            this.dgrListaPacientes.TabIndex = 8;
            // 
            // btnLimpiarMedicos
            // 
            this.btnLimpiarMedicos.Location = new System.Drawing.Point(338, 5);
            this.btnLimpiarMedicos.Name = "btnLimpiarMedicos";
            this.btnLimpiarMedicos.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiarMedicos.TabIndex = 3;
            this.btnLimpiarMedicos.Text = "Limpiar";
            this.btnLimpiarMedicos.UseVisualStyleBackColor = true;
            this.btnLimpiarMedicos.Click += new System.EventHandler(this.btnLimpiarMedicos_Click);
            // 
            // lblListaMedicos
            // 
            this.lblListaMedicos.AutoSize = true;
            this.lblListaMedicos.Location = new System.Drawing.Point(8, 30);
            this.lblListaMedicos.Name = "lblListaMedicos";
            this.lblListaMedicos.Size = new System.Drawing.Size(98, 13);
            this.lblListaMedicos.TabIndex = 8;
            this.lblListaMedicos.Text = "Listado de medicos";
            // 
            // dgrListaMedicos
            // 
            this.dgrListaMedicos.AllowUserToAddRows = false;
            this.dgrListaMedicos.AllowUserToDeleteRows = false;
            this.dgrListaMedicos.AllowUserToResizeRows = false;
            this.dgrListaMedicos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrListaMedicos.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgrListaMedicos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dgrListaMedicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrListaMedicos.Location = new System.Drawing.Point(8, 46);
            this.dgrListaMedicos.MultiSelect = false;
            this.dgrListaMedicos.Name = "dgrListaMedicos";
            this.dgrListaMedicos.ReadOnly = true;
            this.dgrListaMedicos.RowHeadersWidth = 25;
            this.dgrListaMedicos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrListaMedicos.Size = new System.Drawing.Size(405, 149);
            this.dgrListaMedicos.StandardTab = true;
            this.dgrListaMedicos.TabIndex = 4;
            this.dgrListaMedicos.SelectionChanged += new System.EventHandler(this.dgrListaMedicos_SelectionChanged);
            // 
            // btnTurnoNuevo
            // 
            this.btnTurnoNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTurnoNuevo.Location = new System.Drawing.Point(213, 407);
            this.btnTurnoNuevo.Name = "btnTurnoNuevo";
            this.btnTurnoNuevo.Size = new System.Drawing.Size(200, 23);
            this.btnTurnoNuevo.TabIndex = 9;
            this.btnTurnoNuevo.Text = "Turno nuevo";
            this.btnTurnoNuevo.UseVisualStyleBackColor = true;
            this.btnTurnoNuevo.Click += new System.EventHandler(this.btnTurnoNuevo_Click);
            // 
            // btnCambiarTurno
            // 
            this.btnCambiarTurno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCambiarTurno.Location = new System.Drawing.Point(6, 407);
            this.btnCambiarTurno.Name = "btnCambiarTurno";
            this.btnCambiarTurno.Size = new System.Drawing.Size(200, 23);
            this.btnCambiarTurno.TabIndex = 11;
            this.btnCambiarTurno.Text = "Cambiar turno";
            this.btnCambiarTurno.UseVisualStyleBackColor = true;
            this.btnCambiarTurno.Click += new System.EventHandler(this.btnCambiarTurno_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSalir.Location = new System.Drawing.Point(6, 436);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(200, 23);
            this.btnSalir.TabIndex = 12;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // dgrTurnosPorMedico
            // 
            this.dgrTurnosPorMedico.AllowUserToAddRows = false;
            this.dgrTurnosPorMedico.AllowUserToDeleteRows = false;
            this.dgrTurnosPorMedico.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrTurnosPorMedico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrTurnosPorMedico.Location = new System.Drawing.Point(419, 3);
            this.dgrTurnosPorMedico.MultiSelect = false;
            this.dgrTurnosPorMedico.Name = "dgrTurnosPorMedico";
            this.dgrTurnosPorMedico.ReadOnly = true;
            this.dgrTurnosPorMedico.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrTurnosPorMedico.Size = new System.Drawing.Size(598, 456);
            this.dgrTurnosPorMedico.StandardTab = true;
            this.dgrTurnosPorMedico.TabIndex = 10;
            // 
            // btnBuscarMedico
            // 
            this.btnBuscarMedico.Enabled = false;
            this.btnBuscarMedico.Location = new System.Drawing.Point(257, 5);
            this.btnBuscarMedico.Name = "btnBuscarMedico";
            this.btnBuscarMedico.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarMedico.TabIndex = 2;
            this.btnBuscarMedico.Text = "Buscar";
            this.btnBuscarMedico.UseVisualStyleBackColor = true;
            this.btnBuscarMedico.Click += new System.EventHandler(this.btnBuscarMedico_Click);
            // 
            // txtBusquedaMedico
            // 
            this.txtBusquedaMedico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBusquedaMedico.Location = new System.Drawing.Point(8, 7);
            this.txtBusquedaMedico.Name = "txtBusquedaMedico";
            this.txtBusquedaMedico.Size = new System.Drawing.Size(243, 20);
            this.txtBusquedaMedico.TabIndex = 1;
            this.txtBusquedaMedico.TextChanged += new System.EventHandler(this.txtBusquedaMedico_TextChanged);
            // 
            // tabPacientes
            // 
            this.tabPacientes.Controls.Add(this.btnCancelarCambios);
            this.tabPacientes.Controls.Add(this.btnGuardarCambios);
            this.tabPacientes.Controls.Add(this.btnSalir2);
            this.tabPacientes.Controls.Add(this.btnEditarPaciente);
            this.tabPacientes.Controls.Add(this.btnNuevoPaciente);
            this.tabPacientes.Controls.Add(this.dgrPacientes);
            this.tabPacientes.Controls.Add(this.lblObservaciones);
            this.tabPacientes.Controls.Add(this.txtObservaciones);
            this.tabPacientes.Controls.Add(this.dteFechaNacimiento);
            this.tabPacientes.Controls.Add(this.lblNombre);
            this.tabPacientes.Controls.Add(this.lblApellido);
            this.tabPacientes.Controls.Add(this.lblDNI);
            this.tabPacientes.Controls.Add(this.lblFechaNacimiento);
            this.tabPacientes.Controls.Add(this.lblDomicilio);
            this.tabPacientes.Controls.Add(this.lblTelefono);
            this.tabPacientes.Controls.Add(this.txtNombre);
            this.tabPacientes.Controls.Add(this.txtApellido);
            this.tabPacientes.Controls.Add(this.txtDNI);
            this.tabPacientes.Controls.Add(this.txtDomicilio);
            this.tabPacientes.Controls.Add(this.txtTelefono);
            this.tabPacientes.Location = new System.Drawing.Point(4, 22);
            this.tabPacientes.Name = "tabPacientes";
            this.tabPacientes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPacientes.Size = new System.Drawing.Size(1020, 467);
            this.tabPacientes.TabIndex = 1;
            this.tabPacientes.Text = "Pacientes";
            this.tabPacientes.UseVisualStyleBackColor = true;
            // 
            // btnCancelarCambios
            // 
            this.btnCancelarCambios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelarCambios.Enabled = false;
            this.btnCancelarCambios.Location = new System.Drawing.Point(9, 367);
            this.btnCancelarCambios.Name = "btnCancelarCambios";
            this.btnCancelarCambios.Size = new System.Drawing.Size(203, 23);
            this.btnCancelarCambios.TabIndex = 11;
            this.btnCancelarCambios.Text = "Cancelar Cambios";
            this.btnCancelarCambios.UseVisualStyleBackColor = true;
            this.btnCancelarCambios.Click += new System.EventHandler(this.btnCancelarCambios_Click);
            // 
            // btnGuardarCambios
            // 
            this.btnGuardarCambios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGuardarCambios.Enabled = false;
            this.btnGuardarCambios.Location = new System.Drawing.Point(9, 338);
            this.btnGuardarCambios.Name = "btnGuardarCambios";
            this.btnGuardarCambios.Size = new System.Drawing.Size(203, 23);
            this.btnGuardarCambios.TabIndex = 10;
            this.btnGuardarCambios.Text = "Guardar Cambios";
            this.btnGuardarCambios.UseVisualStyleBackColor = true;
            this.btnGuardarCambios.Click += new System.EventHandler(this.btnGuardarCambios_Click);
            // 
            // btnSalir2
            // 
            this.btnSalir2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSalir2.Location = new System.Drawing.Point(9, 436);
            this.btnSalir2.Name = "btnSalir2";
            this.btnSalir2.Size = new System.Drawing.Size(203, 23);
            this.btnSalir2.TabIndex = 14;
            this.btnSalir2.Text = "Salir";
            this.btnSalir2.UseVisualStyleBackColor = true;
            this.btnSalir2.Click += new System.EventHandler(this.btnSalir2_Click);
            // 
            // btnEditarPaciente
            // 
            this.btnEditarPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditarPaciente.Location = new System.Drawing.Point(9, 309);
            this.btnEditarPaciente.Name = "btnEditarPaciente";
            this.btnEditarPaciente.Size = new System.Drawing.Size(203, 23);
            this.btnEditarPaciente.TabIndex = 13;
            this.btnEditarPaciente.Text = "Editar Paciente";
            this.btnEditarPaciente.UseVisualStyleBackColor = true;
            this.btnEditarPaciente.Click += new System.EventHandler(this.btnEditarPaciente_Click);
            // 
            // btnNuevoPaciente
            // 
            this.btnNuevoPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNuevoPaciente.Enabled = false;
            this.btnNuevoPaciente.Location = new System.Drawing.Point(9, 280);
            this.btnNuevoPaciente.Name = "btnNuevoPaciente";
            this.btnNuevoPaciente.Size = new System.Drawing.Size(203, 23);
            this.btnNuevoPaciente.TabIndex = 8;
            this.btnNuevoPaciente.Text = "Nuevo Paciente";
            this.btnNuevoPaciente.UseVisualStyleBackColor = true;
            this.btnNuevoPaciente.Click += new System.EventHandler(this.btnNuevoPaciente_Click);
            // 
            // dgrPacientes
            // 
            this.dgrPacientes.AllowUserToAddRows = false;
            this.dgrPacientes.AllowUserToDeleteRows = false;
            this.dgrPacientes.AllowUserToResizeRows = false;
            this.dgrPacientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrPacientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrPacientes.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgrPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrPacientes.Location = new System.Drawing.Point(218, 3);
            this.dgrPacientes.MultiSelect = false;
            this.dgrPacientes.Name = "dgrPacientes";
            this.dgrPacientes.ReadOnly = true;
            this.dgrPacientes.RowHeadersWidth = 25;
            this.dgrPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrPacientes.Size = new System.Drawing.Size(794, 458);
            this.dgrPacientes.StandardTab = true;
            this.dgrPacientes.TabIndex = 12;
            // 
            // lblObservaciones
            // 
            this.lblObservaciones.AutoSize = true;
            this.lblObservaciones.Location = new System.Drawing.Point(5, 237);
            this.lblObservaciones.Name = "lblObservaciones";
            this.lblObservaciones.Size = new System.Drawing.Size(78, 13);
            this.lblObservaciones.TabIndex = 37;
            this.lblObservaciones.Text = "Observaciones";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtObservaciones.Location = new System.Drawing.Point(8, 253);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(204, 20);
            this.txtObservaciones.TabIndex = 7;
            // 
            // dteFechaNacimiento
            // 
            this.dteFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dteFechaNacimiento.Location = new System.Drawing.Point(8, 214);
            this.dteFechaNacimiento.Name = "dteFechaNacimiento";
            this.dteFechaNacimiento.Size = new System.Drawing.Size(204, 20);
            this.dteFechaNacimiento.TabIndex = 6;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(5, 42);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 36;
            this.lblNombre.Text = "Nombre";
            // 
            // lblApellido
            // 
            this.lblApellido.AutoSize = true;
            this.lblApellido.Location = new System.Drawing.Point(5, 3);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(44, 13);
            this.lblApellido.TabIndex = 35;
            this.lblApellido.Text = "Apellido";
            // 
            // lblDNI
            // 
            this.lblDNI.AutoSize = true;
            this.lblDNI.Location = new System.Drawing.Point(5, 81);
            this.lblDNI.Name = "lblDNI";
            this.lblDNI.Size = new System.Drawing.Size(26, 13);
            this.lblDNI.TabIndex = 34;
            this.lblDNI.Text = "DNI";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(5, 198);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(108, 13);
            this.lblFechaNacimiento.TabIndex = 33;
            this.lblFechaNacimiento.Text = "Fecha de Nacimiento";
            // 
            // lblDomicilio
            // 
            this.lblDomicilio.AutoSize = true;
            this.lblDomicilio.Location = new System.Drawing.Point(5, 159);
            this.lblDomicilio.Name = "lblDomicilio";
            this.lblDomicilio.Size = new System.Drawing.Size(49, 13);
            this.lblDomicilio.TabIndex = 32;
            this.lblDomicilio.Text = "Domicilio";
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Location = new System.Drawing.Point(5, 120);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(49, 13);
            this.lblTelefono.TabIndex = 31;
            this.lblTelefono.Text = "Telefono";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(8, 58);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(204, 20);
            this.txtNombre.TabIndex = 2;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(8, 19);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(204, 20);
            this.txtApellido.TabIndex = 1;
            this.txtApellido.TextChanged += new System.EventHandler(this.txtApellido_TextChanged);
            // 
            // txtDNI
            // 
            this.txtDNI.Location = new System.Drawing.Point(8, 97);
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.Size = new System.Drawing.Size(204, 20);
            this.txtDNI.TabIndex = 3;
            this.txtDNI.TextChanged += new System.EventHandler(this.txtDNI_TextChanged);
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.Location = new System.Drawing.Point(8, 175);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.Size = new System.Drawing.Size(204, 20);
            this.txtDomicilio.TabIndex = 5;
            this.txtDomicilio.TextChanged += new System.EventHandler(this.txtDomicilio_TextChanged);
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(8, 136);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(204, 20);
            this.txtTelefono.TabIndex = 4;
            this.txtTelefono.TextChanged += new System.EventHandler(this.txtTelefono_TextChanged);
            // 
            // FormularioOperarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 493);
            this.Controls.Add(this.tabFormOperarios);
            this.MinimumSize = new System.Drawing.Size(1022, 520);
            this.Name = "FormularioOperarios";
            this.ShowIcon = false;
            this.Text = "Operarios";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormularioOperarios_FormClosing);
            this.Load += new System.EventHandler(this.FormularioOperarios_Load);
            this.tabFormOperarios.ResumeLayout(false);
            this.tabTurnos.ResumeLayout(false);
            this.tabTurnos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrListaPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrListaMedicos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTurnosPorMedico)).EndInit();
            this.tabPacientes.ResumeLayout(false);
            this.tabPacientes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPacientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabFormOperarios;
        private System.Windows.Forms.TabPage tabTurnos;
        private System.Windows.Forms.TabPage tabPacientes;
        private System.Windows.Forms.DataGridView dgrTurnosPorMedico;
        private System.Windows.Forms.Button btnBuscarMedico;
        private System.Windows.Forms.TextBox txtBusquedaMedico;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnTurnoNuevo;
        private System.Windows.Forms.Button btnCambiarTurno;
        private System.Windows.Forms.DataGridView dgrListaMedicos;
        private System.Windows.Forms.Label lblListaMedicos;
        private System.Windows.Forms.Button btnLimpiarMedicos;
        private System.Windows.Forms.Label lblListaPacientes;
        private System.Windows.Forms.DataGridView dgrListaPacientes;
        private System.Windows.Forms.Button btnLimpiarPacientes;
        private System.Windows.Forms.Button btnBuscarPaciente;
        private System.Windows.Forms.TextBox txtBusquedaPaciente;
        private System.Windows.Forms.DataGridView dgrPacientes;
        private System.Windows.Forms.Button btnCancelarCambios;
        private System.Windows.Forms.Button btnGuardarCambios;
        private System.Windows.Forms.Button btnSalir2;
        private System.Windows.Forms.Button btnEditarPaciente;
        private System.Windows.Forms.Button btnNuevoPaciente;
        private System.Windows.Forms.Label lblObservaciones;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.DateTimePicker dteFechaNacimiento;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblDNI;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.Label lblDomicilio;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtDNI;
        private System.Windows.Forms.TextBox txtDomicilio;
        private System.Windows.Forms.TextBox txtTelefono;
    }
}