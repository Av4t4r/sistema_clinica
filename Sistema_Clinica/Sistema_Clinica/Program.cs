﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace Sistema_Clinica
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!File.Exists(Directory.GetCurrentDirectory() + "\\Sistema_Clinica.exe.config"))
            {
                try 
                {
                    //Si no se encuentra el archivo de configuración que acompaña al ejecutable, se lo genera con la configuración predeterminada
                    MessageBox.Show("No se encuentra el archivo de configuración, se generara el predeterminado a continuación.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    string conName = "Sistema_Clinica.Properties.Settings.Sistema_ClinicaConnectionString";
                    string conString = "Data Source=AV4T4R-DESKTOP;Initial Catalog=Sistema_Clinica;Integrated Security=True;Asynchronous Processing=True;Connect Timeout=5;Encrypt=False;ConnectRetryInterval=5";
                    string providerName = "System.Data.SqlClient";
                    ConnectionStringSettings connStrSettings = new ConnectionStringSettings();
                    connStrSettings.Name = conName;
                    connStrSettings.ConnectionString = conString;
                    connStrSettings.ProviderName = providerName;
                    config.ConnectionStrings.ConnectionStrings.Add(connStrSettings);
                    config.Save();
                }
                catch (UnauthorizedAccessException ex)
                { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            if (!File.Exists(Directory.GetCurrentDirectory() + "\\privatekey.xml") || (!File.Exists(Directory.GetCurrentDirectory() + "\\publickey.xml")))
            {
                try
                {
                    MessageBox.Show("No se encuentra el archivo de encriptación, se generara el predeterminado a continuación.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    HashPass.generarRSAPair();
                }
                catch (UnauthorizedAccessException ex)
                { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            Application.Run(new LoginSystem());
        }
    }
}
