﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Clinica
{
    public partial class FormularioTurnoNuevo : Form
    {
        string ID_Operario, ID_Paciente, ID_Medico;

        public FormularioTurnoNuevo(string ID_Operario, string ID_Paciente, string ID_Medico)
        {
            InitializeComponent();
            this.ID_Medico = ID_Medico;
            this.ID_Operario = ID_Operario;
            this.ID_Paciente = ID_Paciente;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(CRUD.nuevoTurno(ID_Operario, ID_Paciente, ID_Medico, mthFecha.SelectionStart, txtHorario.Text, txtMotivo.Text, txtObservaciones.Text), "Operación exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.FormClosing -= FormularioTurnoNuevo_FormClosing; //Desuscribimos el handler del evento FormClosing asi no pregunta si queremos salir
                this.Close();
            }
            catch (FechaFueraDeRango ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (ArgumentNullException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (FormatException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (TurnoNoExistente ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtHorario_TextChanged(object sender, EventArgs e)
        {
            if (txtHorario.Text != "" && txtMotivo.Text != "")
            {
                btnGuardar.Enabled = true;
            }
            else
            {
                btnGuardar.Enabled = false;
            }
        }

        private void txtMotivo_TextChanged(object sender, EventArgs e)
        {
            if (txtHorario.Text != "" && txtMotivo.Text != "")
            {
                btnGuardar.Enabled = true;
            }
            else
            {
                btnGuardar.Enabled = false;
            }
        }

        private void FormularioTurnoNuevo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea cancelar?", "Cancelar Acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            { e.Cancel = true; }
        }
    }
}
