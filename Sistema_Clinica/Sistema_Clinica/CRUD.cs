﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Sistema_Clinica
{
    public static class CRUD
    {
        #region declaraciones
        private static Sistema_ClinicaDataSet sistema_ClinicaDataSet = new Sistema_ClinicaDataSet();
        private static Sistema_ClinicaDataSetTableAdapters.MedicosTableAdapter medicosTableAdapter = new Sistema_ClinicaDataSetTableAdapters.MedicosTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.OperariosTableAdapter operariosTableAdapter = new Sistema_ClinicaDataSetTableAdapters.OperariosTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.PacientesTableAdapter pacientesTableAdapter = new Sistema_ClinicaDataSetTableAdapters.PacientesTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.TurnosTableAdapter turnosTableAdapter = new Sistema_ClinicaDataSetTableAdapters.TurnosTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getTurnosMedicoTableAdapter getTurnosMedicoTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getTurnosMedicoTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getTurnosMedicoByDateTableAdapter getTurnosMedicoByDateTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getTurnosMedicoByDateTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getTurnosPacienteTableAdapter getTurnosPacienteTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getTurnosPacienteTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getPacientesTableAdapter getPacientesTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getPacientesTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getMedicosTableAdapter getMedicosTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getMedicosTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getOperariosTableAdapter getOperariosTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getOperariosTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getOperariosHistorialTableAdapter getOperariosHistorialTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getOperariosHistorialTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getPacientesHistorialTableAdapter getPacientesHistorialTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getPacientesHistorialTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getMedicosHistorialTableAdapter getMedicosHistorialTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getMedicosHistorialTableAdapter();
        private static Sistema_ClinicaDataSetTableAdapters.getTurnosTableAdapter getTurnosTableAdapter = new Sistema_ClinicaDataSetTableAdapters.getTurnosTableAdapter();
        private static string regexDNI = "[^0-9]"; //regex para solamente incluir números
        private static string regexNumeros = "[0-9]"; //regex para excluir números
        private static string regexHorario = @"^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"; //Regex para verificar el horario de un turno
        #endregion

        #region getters & stored procedures
        public static Sistema_ClinicaDataSet.getTurnosDataTable getTurnos(DateTime fechaInicial, DateTime fechaFinal)
        {
            return getTurnosTableAdapter.GetData(fechaInicial, fechaFinal);
        }
        public static Sistema_ClinicaDataSet.getPacientesDataTable getPacientes()
        {
            return getPacientesTableAdapter.GetData();
        }
        public static Sistema_ClinicaDataSet.getMedicosDataTable getMedicos()
        {
            return getMedicosTableAdapter.GetData();
        }
        public static Sistema_ClinicaDataSet.getOperariosDataTable getOperarios()
        {
            return getOperariosTableAdapter.GetData();
        }
        public static Sistema_ClinicaDataSet.getPacientesHistorialDataTable getPacientesHistorial()
        {
            return getPacientesHistorialTableAdapter.GetData();
        }
        public static Sistema_ClinicaDataSet.getMedicosHistorialDataTable getMedicosHistorial()
        {
            return getMedicosHistorialTableAdapter.GetData();
        }
        public static Sistema_ClinicaDataSet.getOperariosHistorialDataTable getOperariosHistorial()
        {
            return getOperariosHistorialTableAdapter.GetData();
        }
        public static Sistema_ClinicaDataSet.PacientesRow getPacienteByDNI (string DNI)
        {
            if (DNI == String.Empty)
            {
                throw new ArgumentNullException(DNI, "El DNI no puede estar vacío.");
            }
            else
            {
                try
                {
                    //Y lo buscamos entre todos los pacientes
                    foreach (Sistema_ClinicaDataSet.PacientesRow fila in pacientesTableAdapter.GetData())
                    {
                        if (fila["DNI"].Equals(DNI))
                        {
                            //si lo encontramos, lo retornamos
                            return fila;
                        }
                    }
                    //Si no, excepcion avisando que no se encontro
                    throw new PacienteNoExistente("El paciente no se encuentra");
                }
                catch (FormatException)
                {
                    throw new FormatException("El DNI solamente puede contener números");
                }
            }
        }
        public static Sistema_ClinicaDataSet.OperariosRow getOperarioByDNI(string DNI)
        {
            if (DNI == String.Empty)
            {
                throw new ArgumentNullException(DNI, "El DNI no puede estar vacío.");
            }
            else
            {
                try
                {
                    foreach (Sistema_ClinicaDataSet.OperariosRow fila in operariosTableAdapter.GetData())
                    {
                        //Lo buscamos entre todos los operarios
                        if (fila["DNI"].Equals(DNI))
                        {
                            //Si lo encontramos lo retornamos
                            return fila;
                        }
                    }
                    //si no, excepcion avisando que no se encuentra
                    throw new OperarioNoExistente("El operario no se encuentra");
                }
                catch (FormatException)
                {
                    throw new FormatException("El DNI solamente puede contener números");
                }
            }
        }
        public static Sistema_ClinicaDataSet.MedicosRow getMedicoByDNI(string DNI)
        {
            if (DNI == String.Empty)
            {
                throw new ArgumentNullException(DNI, "El DNI no puede estar vacío.");
            }
            else
            {
                try
                {
                    foreach (Sistema_ClinicaDataSet.MedicosRow fila in medicosTableAdapter.GetData())
                    {
                        //Lo buscamos entre todos los medicos
                        if (fila["DNI"].Equals(DNI))
                        {
                            //si lo encontramos lo devolvemos
                            return fila;
                        }
                    }
                    //Si no, excepcion avisando que no se encuentra
                    throw new MedicoNoExistente("El medico no se encuentra");
                }
                catch (FormatException)
                {
                    throw new FormatException("El DNI solamente puede contener números");
                }
            }
        }
        public static Sistema_ClinicaDataSet.getTurnosMedicoDataTable getTurnosMedico(string ID_Medico)
        {
            if (ID_Medico == String.Empty)
            { throw new ArgumentNullException(ID_Medico, "El ID no puede estar vacío"); }
            try
            {
                return getTurnosMedicoTableAdapter.GetData(Int32.Parse(ID_Medico));
            }
            catch (FormatException)
            { throw new FormatException("El ID solo puede contener numeros."); }
        }
        public static Sistema_ClinicaDataSet.getTurnosMedicoByDateDataTable getTurnosMedicoByDate (string ID_Medico, DateTime fecha)
        {
            if (ID_Medico == String.Empty)
            { throw new ArgumentNullException(ID_Medico, "El ID no puede estar vacío"); }
            try
            {
                return getTurnosMedicoByDateTableAdapter.GetData(Int32.Parse(ID_Medico), fecha);
            }
            catch (FormatException)
            { throw new FormatException("El ID Solo puede contener numeros."); }
        }
        public static Sistema_ClinicaDataSet.getTurnosPacienteDataTable getTurnosPaciente (string ID_Paciente)
        {
            if (ID_Paciente == String.Empty)
            { throw new ArgumentNullException(ID_Paciente, "El ID no puede estar vacío"); }
            try
            {
                return getTurnosPacienteTableAdapter.GetData(Int32.Parse(ID_Paciente));
            }
            catch (FormatException)
            { throw new FormatException("El ID solo puede contener numeros."); }
        }
        #endregion

        #region seccion "nuevos registros"
        public static string nuevoPaciente(string apellido, string nombre, string DNI, DateTime fecha_Nacimiento, string domicilio, string telefono, string observaciones, string ID_Operario)
        {
            DateTime fechaActual = DateTime.Today;
            if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else
            {
                foreach (DataRow filas in pacientesTableAdapter.GetData())
                {
                    if (filas["DNI"].Equals(DNI))
                    { throw new PacienteExistente("El paciente ya existe"); }//Buscamos que el paciente no exista por las dudas
                }
            }
            if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < fechaActual) != true)
            { throw new FechaFueraDeRango("El paciente debe tener al menos 1 día de edad."); }
            else
            {
                try //Intentamos crear el paciente nuevo
                {
                    Sistema_ClinicaDataSet.PacientesRow pacienteNuevo = sistema_ClinicaDataSet.Pacientes.NewPacientesRow();
                    pacienteNuevo.Apellido = apellido;
                    pacienteNuevo.Nombre = nombre;
                    pacienteNuevo.Domicilio = domicilio;
                    pacienteNuevo.Telefono = telefono;
                    pacienteNuevo.DNI = DNI;
                    pacienteNuevo.Observaciones = observaciones;
                    pacienteNuevo.ID_Operario = Int32.Parse(ID_Operario);
                    pacienteNuevo.Fecha_Nacimiento = fecha_Nacimiento;
                    pacienteNuevo.Actividad = true;
                    sistema_ClinicaDataSet.Pacientes.Rows.Add(pacienteNuevo);
                    pacientesTableAdapter.Update(pacienteNuevo);
                    return "¡Paciente registrado exitosamente!";
                }
                catch { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
            }
        }
        public static string nuevoMedico(string apellido, string nombre, string DNI, string domicilio, string telefono, DateTime fecha_Nacimiento, string observaciones, 
            string nro_Matricula, string contraseña, string confirmarContraseña)
        {
            DateTime fechaActual = DateTime.Today;

            if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else
            {

                foreach (DataRow fila in medicosTableAdapter.GetData())
                {
                    if (fila["DNI"].Equals(DNI)) //Buscamos la columna "DNI" (es el usuario)
                    {
                        //si existe se tira una excepción
                        throw new UsuarioExistente("El usuario ya existe");
                    }
                }
            }
            if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < fechaActual) != true)
            { throw new FechaFueraDeRango("El médico debe tener al menos 18 años."); }
            else if ((fechaActual.Year - fecha_Nacimiento.Year) <= 17)
            { throw new FechaFueraDeRango("El médico debe tener al menos 18 años."); }
            else if (nro_Matricula == String.Empty)
            { throw new ArgumentNullException(nro_Matricula, "La matricula no puede estar vacía"); }
            else if (contraseña == String.Empty)
            { throw new ContraseñaNoRespetaFormato("La contraseña no puede estar vacía."); }
            else if (contraseña.Length < 8 || contraseña.Length > 64)
            { throw new ContraseñaNoRespetaFormato("La contraseña debe contener entre 8 y 64 caracteres."); }
            //Se confirma que las contraseñas sean iguales
            else if (contraseña != confirmarContraseña)
            { throw new ContraseñaNoCoincide("Las contraseñas no coinciden"); }
            //Si todo esta bien, creamos el usuario
            else
            {
                try
                {
                    string nro_MatriculaEncriptada = HashPass.RSAEncrypt(nro_Matricula, false);
                    string contraseñaEncriptada = HashPass.encriptarContraseña(contraseña);
                    //llenamos los datos
                    Sistema_ClinicaDataSet.MedicosRow medicoNuevo = sistema_ClinicaDataSet.Medicos.NewMedicosRow();
                    medicoNuevo.Contraseña = contraseñaEncriptada;
                    medicoNuevo.Apellido = apellido;
                    medicoNuevo.Nombre = nombre;
                    medicoNuevo.DNI = DNI;
                    medicoNuevo.Domicilio = domicilio;
                    medicoNuevo.Telefono = telefono;
                    medicoNuevo.Fecha_Nacimiento = fecha_Nacimiento;
                    medicoNuevo.Nro_Matricula = nro_MatriculaEncriptada;
                    medicoNuevo.Observaciones = observaciones;
                    medicoNuevo.Actividad = true;
                    //y los guardamos a la base de datos
                    sistema_ClinicaDataSet.Medicos.Rows.Add(medicoNuevo);
                    medicosTableAdapter.Update(medicoNuevo);
                    return ("¡Médico registrado exitosamente!");
                }
                catch { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
            }
        }
        public static string nuevoOperario(string apellido, string nombre, string DNI, string domicilio, string telefono, DateTime fecha_Nacimiento, string observaciones, 
            string contraseña, string confirmarContraseña)
        {
            DateTime fechaActual = DateTime.Today;

            if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else
            {
                foreach (DataRow fila in operariosTableAdapter.GetData())
                {
                    if (fila["DNI"].Equals(DNI))
                    {
                        //si existe se tira una excepción
                        throw new UsuarioExistente("El usuario ya existe");
                    }
                }
            }
            if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < fechaActual) != true)
            { throw new OverflowException("El operario debe tener al menos 16 años.");}
            else if ((fechaActual.Year - fecha_Nacimiento.Year) <= 15)
            { throw new OverflowException("El operario debe tener al menos 16 años."); }
            else if (contraseña == String.Empty)
            { throw new ContraseñaNoRespetaFormato("La contraseña no puede estar vacía."); }
            else if (contraseña.Length < 8 || contraseña.Length > 64)
            { throw new ContraseñaNoRespetaFormato("La contraseña debe contener entre 8 y 64 caracteres."); }
            //Se confirma que las contraseñas sean iguales
            else if (contraseña != confirmarContraseña)
            { throw new ContraseñaNoCoincide("Las contraseñas no coinciden"); }
            //Si todo esta bien, creamos el operario
            else
            {
                try
                {
                    string contraseñaEncriptada = HashPass.encriptarContraseña(contraseña);
                    Sistema_ClinicaDataSet.OperariosRow operarioNuevo = sistema_ClinicaDataSet.Operarios.NewOperariosRow();
                    operarioNuevo.Contraseña = contraseñaEncriptada;
                    operarioNuevo.Nombre = nombre;
                    operarioNuevo.Apellido = apellido;
                    operarioNuevo.DNI = DNI;
                    operarioNuevo.Domicilio = domicilio;
                    operarioNuevo.Telefono = telefono;
                    operarioNuevo.Fecha_Nacimiento = fecha_Nacimiento;
                    operarioNuevo.Observaciones = observaciones;
                    operarioNuevo.Actividad = true;
                    sistema_ClinicaDataSet.Operarios.Rows.Add(operarioNuevo);
                    operariosTableAdapter.Update(operarioNuevo);
                    return ("¡Operario registrado exitosamente!");
                }
                catch { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
            }   
        }
        public static string nuevoTurno(string ID_Operario, string ID_Paciente, string ID_Medico, DateTime fecha_Turno, string horario_Turno, string motivo_Consulta, string observaciones)
        {
            if (fecha_Turno.Date < DateTime.Today) //Verificamos que no intenten dar un turno para un día anterior a la fecha actual
            { throw new FechaFueraDeRango("El turno no puede ser anterior a la fecha actual."); }
            else if (horario_Turno == String.Empty)
            { throw new ArgumentNullException(horario_Turno, "Debe completar el horario."); }
            else if (!Regex.IsMatch(horario_Turno, regexHorario, RegexOptions.Singleline))
            { throw new FechaFueraDeRango("El horario debe respetar el formato HH:MM"); }
            else if (motivo_Consulta == String.Empty)
            { throw new ArgumentNullException(motivo_Consulta, "Debe completar el motivo."); }
            else if (ID_Operario == String.Empty)
            { throw new ArgumentNullException(ID_Operario, "El ID no puede estar vacío"); }
            else if (ID_Paciente == String.Empty)
            { throw new ArgumentNullException(ID_Paciente, "El ID no puede estar vacío"); }
            else if (ID_Medico == String.Empty)
            { throw new ArgumentNullException(ID_Medico, "El ID no puede estar vacío"); }
            //y que los datos esten completos
            else
            {
                //Intentamos crear el turno nuevo
                try
                {
                    Sistema_ClinicaDataSet.TurnosRow turnoNuevo = sistema_ClinicaDataSet.Turnos.NewTurnosRow();
                    turnoNuevo.ID_Operario = Int32.Parse(ID_Operario);
                    turnoNuevo.ID_Paciente = Int32.Parse(ID_Paciente);
                    turnoNuevo.ID_Medico = Int32.Parse(ID_Medico);
                    turnoNuevo.Fecha_Turno = fecha_Turno;
                    turnoNuevo.Horario_Turno = horario_Turno;
                    turnoNuevo.Motivo_Consulta = motivo_Consulta;
                    turnoNuevo.Actividad = true; //Actividad se asume como verdadera al instante de pedir el turno
                    turnoNuevo.Asistencia = false; //Asistencia se asume como falsa, ya que se piden con antelacion los turnos
                    turnoNuevo.Observaciones = observaciones;
                    sistema_ClinicaDataSet.Turnos.Rows.Add(turnoNuevo);
                    turnosTableAdapter.Update(turnoNuevo);
                    return "¡Turno registrado exitosamente!";
                }
                //En el caso de que algo falle, se avisa con una excepcion y se retorna la ejecución al stack anterior
                catch { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
            }
        }
        #endregion

        #region seccion "editar registros"
        public static string editarTurno(string ID_Turno, DateTime fecha_Turno, string horario_Turno, string motivo_Consulta, string observaciones, bool actividad, bool asistencia)
        {
            if (ID_Turno == String.Empty)
            { throw new ArgumentNullException(ID_Turno, "El ID no puede estar vacio."); }
            else if (fecha_Turno.Date < DateTime.Today) //Verificamos que no intenten dar un turno para un día anterior a la fecha actual
            { throw new FechaFueraDeRango("El turno no puede ser anterior a la fecha actual."); }
            else if (horario_Turno == String.Empty)
            { throw new ArgumentNullException(horario_Turno, "Debe completar el horario."); }
            else if (!Regex.IsMatch(horario_Turno, regexHorario, RegexOptions.Singleline))
            { throw new FechaFueraDeRango("El horario debe respetar el formato HH:MM"); }
            else if (motivo_Consulta == String.Empty)
            { throw new ArgumentNullException(motivo_Consulta, "Debe completar el motivo."); }
            //y que los datos esten completos
            else
            {
                try
                {
                    //Parseamos el ID
                    int ID = Int32.Parse(ID_Turno);
                    foreach (DataRow fila in turnosTableAdapter.GetData())
                    {
                        //Lo buscamos entre todos los turnos
                        if (fila["ID_Turno"].Equals(ID))
                        {
                            //Cuando lo encontramos, actualizamos la informacion
                            fila["Fecha_Turno"] = fecha_Turno;
                            fila["Horario_Turno"] = horario_Turno;
                            fila["Motivo_Consulta"] = motivo_Consulta;
                            fila["Actividad"] = actividad;
                            fila["Observaciones"] = observaciones;
                            fila["Asistencia"] = asistencia;
                            turnosTableAdapter.Update(fila);
                            return "¡Turno editado exitosamente!";
                        }
                    }
                }
                catch (FormatException)
                { throw new FormatException("El ID del turno solamente puede ser numérico"); }
                catch (Exception)
                { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
                //Si no lo encontramos, excepcion avisando
                throw new TurnoNoExistente("El turno solicitado no existe.");
            }
        }
        public static string editarPaciente(string ID_Paciente, string apellido, string nombre, string DNI, DateTime fecha_Nacimiento, string domicilio, string telefono, 
            string observaciones)
        {
            if (ID_Paciente == String.Empty)
            { throw new ArgumentNullException(ID_Paciente, "Debe elegir un paciente"); }
            else if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < DateTime.Today) != true)
            { throw new FechaFueraDeRango("El paciente debe tener al menos 1 día de edad."); }
            else
            {
                int ID = Int32.Parse(ID_Paciente);
                try
                {
                    //Buscamos el paciente con el ID
                    foreach (DataRow fila in pacientesTableAdapter.GetData())
                    {
                        if (fila["ID_Paciente"].Equals(ID))
                        {
                            if (fila["DNI"].ToString() != DNI)
                            {
                                foreach (DataRow filas in pacientesTableAdapter.GetData())
                                {
                                    if (filas["DNI"].Equals(DNI))
                                    { throw new DNIEnUso("El DNI ya se encuentra registrado"); }//Buscamos que el DNI no exista por las dudas
                                }
                                //Actualizamos los datos
                                fila["DNI"] = DNI;
                                fila["Apellido"] = apellido;
                                fila["Nombre"] = nombre;
                                fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                                fila["Domicilio"] = domicilio;
                                fila["Telefono"] = telefono;
                                fila["Observaciones"] = observaciones;
                                pacientesTableAdapter.Update(fila);
                                return "¡Paciente editado exitosamente!";
                            }
                            //Actualizamos los datos
                            fila["Apellido"] = apellido;
                            fila["Nombre"] = nombre;
                            fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                            fila["Domicilio"] = domicilio;
                            fila["Telefono"] = telefono;
                            fila["Observaciones"] = observaciones;
                            pacientesTableAdapter.Update(fila);
                            return "¡Paciente editado exitosamente!";
                        }
                    }
                }
                catch (DNIEnUso)
                { throw new DNIEnUso("El DNI ya se encuentra registrado"); }
                catch (Exception)
                { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
                //Si no encontramos el paciente, excepcion avisando esto
                throw new PacienteNoExistente("El paciente seleccionado no puede ser encontrado.");
            }
        }
        public static string editarMedico(string ID_Medico, string apellido, string nombre, string DNI, string nro_Matricula, DateTime fecha_Nacimiento, string domicilio, 
            string telefono, string observaciones)
        {
            if (ID_Medico == String.Empty)
            { throw new ArgumentNullException(ID_Medico, "Debe elegir un medico"); }
            else if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < DateTime.Today) != true)
            { throw new FechaFueraDeRango("El médico debe tener al menos 18 años."); }
            else if ((DateTime.Today.Year - fecha_Nacimiento.Year) <= 17)
            { throw new FechaFueraDeRango("El médico debe tener al menos 18 años."); }
            else if (nro_Matricula == String.Empty)
            { throw new ArgumentNullException(nro_Matricula, "La matricula no puede estar vacía"); }
            else
            {
                int ID = Int32.Parse(ID_Medico);
                try
                {
                    foreach (DataRow fila in medicosTableAdapter.GetData())
                    {
                        if (fila["ID_Medico"].Equals(ID)) //Buscamos el medico con el ID
                        {
                            if (fila["DNI"].ToString() != DNI)
                            {
                                foreach (DataRow filas in medicosTableAdapter.GetData())
                                {
                                    if (filas["DNI"].Equals(DNI))
                                    { throw new DNIEnUso("El DNI ya se encuentra registrado"); }//Buscamos que el DNI no exista por las dudas
                                }
                                //Actualizamos los datos
                                fila["Apellido"] = apellido;
                                fila["Nombre"] = nombre;
                                fila["DNI"] = DNI;
                                fila["Nro_Matricula"] = nro_Matricula;
                                fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                                fila["Domicilio"] = domicilio;
                                fila["Telefono"] = telefono;
                                fila["Observaciones"] = observaciones;
                                medicosTableAdapter.Update(fila);
                                return "¡Médico editado exitosamente!";
                            }
                            //Actualizamos los datos
                            fila["Apellido"] = apellido;
                            fila["Nombre"] = nombre;
                            fila["Nro_Matricula"] = nro_Matricula;
                            fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                            fila["Domicilio"] = domicilio;
                            fila["Telefono"] = telefono;
                            fila["Observaciones"] = observaciones;
                            medicosTableAdapter.Update(fila);
                            return "¡Médico editado exitosamente!";
                        }
                    }
                }
                catch (DNIEnUso)
                { throw new DNIEnUso("El DNI ya se encuentra registrado"); }
                catch (Exception)
                { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
                //Si no encontramos el medico, excepcion avisando esto
                throw new MedicoNoExistente("El médico buscado no puede ser encontrado");
            }
        }

        public static string editarMedico(string ID_Medico, string apellido, string nombre, string DNI, string nro_Matricula, DateTime fecha_Nacimiento, string domicilio,
            string telefono, string observaciones, string contraseña)
        {
            if (ID_Medico == String.Empty)
            { throw new ArgumentNullException(ID_Medico, "Debe elegir un medico"); }
            else if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < DateTime.Today) != true)
            { throw new FechaFueraDeRango("El médico debe tener al menos 18 años."); }
            else if ((DateTime.Today.Year - fecha_Nacimiento.Year) <= 17)
            { throw new FechaFueraDeRango("El médico debe tener al menos 18 años."); }
            else if (nro_Matricula == String.Empty)
            { throw new ArgumentNullException(nro_Matricula, "La matricula no puede estar vacía"); }
            else if (contraseña == String.Empty)
            { throw new ContraseñaNoRespetaFormato("La contraseña no puede estar vacía."); }
            else if (contraseña.Length < 8 || contraseña.Length > 64)
            { throw new ContraseñaNoRespetaFormato("La contraseña debe contener entre 8 y 64 caracteres."); }
            else
            {
                int ID = Int32.Parse(ID_Medico);
                try
                {
                    foreach (DataRow fila in medicosTableAdapter.GetData())
                    {
                        if (fila["ID_Medico"].Equals(ID)) //Buscamos el medico con el ID
                        {
                            if (fila["DNI"].ToString() != DNI)
                            {
                                foreach (DataRow filas in medicosTableAdapter.GetData())
                                {
                                    if (filas["DNI"].Equals(DNI))
                                    { throw new DNIEnUso("El DNI ya se encuentra registrado"); }//Buscamos que el DNI no exista por las dudas
                                }
                                //Actualizamos los datos
                                fila["Apellido"] = apellido;
                                fila["Nombre"] = nombre;
                                fila["DNI"] = DNI;
                                fila["Nro_Matricula"] = nro_Matricula;
                                fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                                fila["Domicilio"] = domicilio;
                                fila["Telefono"] = telefono;
                                fila["Observaciones"] = observaciones;
                                fila["Contraseña"] = HashPass.encriptarContraseña(contraseña);
                                medicosTableAdapter.Update(fila);
                                return "¡Médico editado exitosamente!";
                            }
                            //Actualizamos los datos
                            fila["Apellido"] = apellido;
                            fila["Nombre"] = nombre;
                            fila["Nro_Matricula"] = nro_Matricula;
                            fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                            fila["Domicilio"] = domicilio;
                            fila["Telefono"] = telefono;
                            fila["Observaciones"] = observaciones;
                            fila["Contraseña"] = HashPass.encriptarContraseña(contraseña);
                            medicosTableAdapter.Update(fila);
                            return "¡Médico editado exitosamente!";
                        }
                    }
                }
                catch (DNIEnUso)
                { throw new DNIEnUso("El DNI ya se encuentra registrado"); }
                catch (Exception)
                { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
                //Si no encontramos el medico, excepcion avisando esto
                throw new MedicoNoExistente("El médico buscado no puede ser encontrado");
            }
        }

        public static string editarOperario (string ID_Operario, string apellido, string nombre, string DNI, DateTime fecha_Nacimiento, string domicilio, string telefono, 
            string observaciones)
        {
            if (ID_Operario == String.Empty)
            { throw new ArgumentNullException(ID_Operario, "Debe elegir un operario"); }
            else if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < DateTime.Today) != true)
            { throw new OverflowException("El operario debe tener al menos 16 años."); }
            else if ((DateTime.Today.Year - fecha_Nacimiento.Year) <= 15)
            { throw new OverflowException("El operario debe tener al menos 16 años."); }
            else
            {
                int ID = Int32.Parse(ID_Operario);
                try
                {
                    foreach (DataRow fila in operariosTableAdapter.GetData())
                    {
                        if (fila["ID_Operario"].Equals(ID)) //Buscamos el operario con el ID
                        {
                            if (fila["DNI"].ToString() != DNI)
                            {
                                foreach (DataRow filas in operariosTableAdapter.GetData())
                                {
                                    if (filas["DNI"].Equals(DNI))
                                    { throw new DNIEnUso("El DNI ya se encuentra registrado"); }//Buscamos que el DNI no exista por las dudas
                                }
                                //Actualizamos los datos
                                fila["Apellido"] = apellido;
                                fila["Nombre"] = nombre;
                                fila["DNI"] = DNI;
                                fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                                fila["Domicilio"] = domicilio;
                                fila["Telefono"] = telefono;
                                fila["Observaciones"] = observaciones;
                                operariosTableAdapter.Update(fila);
                                return "¡Operario editado exitosamente!";
                            }
                            //Actualizamos los datos
                            fila["Apellido"] = apellido;
                            fila["Nombre"] = nombre;
                            fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                            fila["Domicilio"] = domicilio;
                            fila["Telefono"] = telefono;
                            fila["Observaciones"] = observaciones;
                            operariosTableAdapter.Update(fila);
                            return "¡Operario editado exitosamente!";
                        }
                    }
                }
                catch (DNIEnUso)
                { throw new DNIEnUso("El DNI ya se encuentra registrado"); }
                catch (Exception)
                { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
                //Si no encontramos el medico, excepcion avisando esto
                throw new OperarioNoExistente("El operario buscado no puede ser encontrado");
            }
        }

        public static string editarOperario(string ID_Operario, string apellido, string nombre, string DNI, DateTime fecha_Nacimiento, string domicilio, string telefono,
            string observaciones, string contraseña)
        {
            if (ID_Operario == String.Empty)
            { throw new ArgumentNullException(ID_Operario, "Debe elegir un operario"); }
            else if (DNI == String.Empty)
            { throw new ArgumentNullException(DNI, "El DNI no puede estar vacío"); }
            else if (DNI.Length < 7 || DNI.Length > 10)
            { throw new FormatException("El DNI debe contener entre 7 y 10 digitos"); }
            else if (Regex.IsMatch(DNI, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El DNI solamente puede contener números"); }
            else if (nombre == String.Empty || apellido == String.Empty || domicilio == String.Empty || telefono == String.Empty)
            { throw new DatosPersonalesVacios("Los datos personales no pueden estar vacíos"); }
            else if (Regex.IsMatch(nombre, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El nombre no puede contener números"); }
            else if (Regex.IsMatch(apellido, regexNumeros, RegexOptions.Singleline))
            { throw new FormatException("El apellido no puede contener números"); }
            else if ((fecha_Nacimiento < DateTime.Today) != true)
            { throw new OverflowException("El operario debe tener al menos 16 años."); }
            else if ((DateTime.Today.Year - fecha_Nacimiento.Year) <= 15)
            { throw new OverflowException("El operario debe tener al menos 16 años."); }
            else if (contraseña == String.Empty)
            { throw new ContraseñaNoRespetaFormato("La contraseña no puede estar vacía."); }
            else if (contraseña.Length < 8 || contraseña.Length > 64)
            { throw new ContraseñaNoRespetaFormato("La contraseña debe contener entre 8 y 64 caracteres."); }
            else
            {
                int ID = Int32.Parse(ID_Operario);
                try
                {
                    foreach (DataRow fila in operariosTableAdapter.GetData())
                    {
                        if (fila["ID_Medico"].Equals(ID)) //Buscamos el paciente con el ID
                        {
                            if (fila["DNI"].ToString() != DNI)
                            {
                                foreach (DataRow filas in operariosTableAdapter.GetData())
                                {
                                    if (filas["DNI"].Equals(DNI))
                                    { throw new DNIEnUso("El DNI ya se encuentra registrado"); }//Buscamos que el DNI no exista por las dudas
                                }
                                //Actualizamos los datos
                                fila["Apellido"] = apellido;
                                fila["Nombre"] = nombre;
                                fila["DNI"] = DNI;
                                fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                                fila["Domicilio"] = domicilio;
                                fila["Telefono"] = telefono;
                                fila["Observaciones"] = observaciones;
                                fila["Contraseña"] = HashPass.encriptarContraseña(contraseña);
                                operariosTableAdapter.Update(fila);
                                return "¡Operario editado exitosamente!";
                            }
                            //Actualizamos los datos
                            fila["Apellido"] = apellido;
                            fila["Nombre"] = nombre;
                            fila["Fecha_Nacimiento"] = fecha_Nacimiento;
                            fila["Domicilio"] = domicilio;
                            fila["Telefono"] = telefono;
                            fila["Observaciones"] = observaciones;
                            fila["Contraseña"] = HashPass.encriptarContraseña(contraseña);
                            operariosTableAdapter.Update(fila);
                            return "¡Operario editado exitosamente!";
                        }
                    }
                }
                catch (DNIEnUso)
                { throw new DNIEnUso("El DNI ya se encuentra registrado"); }
                catch (Exception)
                { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
                //Si no encontramos el medico, excepcion avisando esto
                throw new OperarioNoExistente("El operario buscado no puede ser encontrado");
            }
        }
        #endregion

        #region "seccion "eliminar registros"
        public static string eliminarTurno (string ID_Turno)
        {
            if (ID_Turno == String.Empty)
            { throw new ArgumentNullException(ID_Turno, "El ID no puede estar vacío"); }
            int ID;
            try
            {
                ID = Int32.Parse(ID_Turno);
                foreach (DataRow fila in turnosTableAdapter.GetData())
                {
                    if (fila["ID_Turno"].Equals(ID))
                    {
                        fila["Actividad"] = false;
                        turnosTableAdapter.Update(fila);
                        return "Turno eliminado exitosamente!";
                    }
                }
                throw new TurnoNoExistente("El turno seleccionado no existe!");
            }
            catch
            { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
        }
        public static string eliminarPaciente(string ID_Paciente)
        {
            if (ID_Paciente == String.Empty)
            { throw new ArgumentNullException(ID_Paciente, "El ID no puede estar vacío"); }
            int ID;
            try
            {
                ID = Int32.Parse(ID_Paciente);
                foreach (DataRow fila in pacientesTableAdapter.GetData())
                {
                    if (fila["ID_Paciente"].Equals(ID))
                    {
                        fila["Actividad"] = false;
                        pacientesTableAdapter.Update(fila);
                        return "Paciente eliminado exitosamente!";
                    }
                }
                throw new PacienteNoExistente("El paciente seleccionado no existe!");
            }
            catch
            { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
        }
        public static string eliminarOperario (string ID_Operario)
        {
            if (ID_Operario == String.Empty)
            {
                { throw new ArgumentNullException(ID_Operario, "El ID no puede estar vacío"); }
            }
            int ID;
            try
            {
                ID = Int32.Parse(ID_Operario);
                foreach (DataRow fila in operariosTableAdapter.GetData())
                {
                    if (fila["ID_Operario"].Equals(ID))
                    {
                        fila["Actividad"] = false;
                        operariosTableAdapter.Update(fila);
                        return "Operario eliminado exitosamente!";
                    }
                }
                throw new OperarioNoExistente("El operario buscado no existe.");
            }
            catch
            { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
        }
        public static string eliminarMedico(string ID_Medico)
        {
            if (ID_Medico == String.Empty)
            {
                { throw new ArgumentNullException(ID_Medico, "El ID no puede estar vacío"); }
            }
            int ID;
            try
            {
                ID = Int32.Parse(ID_Medico);
                foreach (DataRow fila in medicosTableAdapter.GetData())
                {
                    if (fila["ID_Medico"].Equals(ID))
                    {
                        fila["Actividad"] = false;
                        medicosTableAdapter.Update(fila);
                        return "Medico eliminado exitosamente!";
                    }
                }
                throw new MedicoNoExistente("El medico buscado no existe.");
            }
            catch
            { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
        }
        #endregion
        public static string login(string username, string contraseña, int tipoUsuario)
        {
            //Confirmamos que este todo completo
            if (username == String.Empty)
            { throw new ArgumentNullException(username, "El usuario no puede estar vacío"); }
            else if (tipoUsuario == -1) // -1 es la constante usada por .NET para "no seleccionado"
            { throw new LoginFail("Debe elegir un tipo de usuario"); }
            else if (Regex.IsMatch(username, regexDNI, RegexOptions.Singleline))
            { throw new FormatException("El usuario solamente puede contener números."); }
            else
            {

                //Segun el rol verificamos los datos contra las tablas de Operario o Medico
                switch (tipoUsuario)
                {
                    case 0:
                        {
                            foreach (DataRow fila in getMedicosTableAdapter.GetData())
                            {
                                //Buscamos en las columnas "Usuario" y "Contraseña" que los datos sean los que estan guardados
                                if (fila["DNI"].Equals(username) && fila["Contraseña"].ToString().Equals(contraseña))
                                {
                                    return ("Datos correctos, cargando");
                                }
                            }
                            throw new LoginFail("Usuario o contraseña incorrectas");
                        }
                    case 1:
                        {
                            foreach (DataRow fila in getOperariosTableAdapter.GetData())
                            {
                                if (fila["DNI"].Equals(username) && fila["Contraseña"].ToString().Equals(contraseña))
                                {
                                    return ("Datos correctos, cargando");
                                }
                            }
                            throw new LoginFail("Usuario o contraseña incorrectas");
                        }
                }
            }
            { throw new Exception("Ocurrio un error inesperado"); } //Si hay algún problema, lo informamos con una excepción
        }

    }
}