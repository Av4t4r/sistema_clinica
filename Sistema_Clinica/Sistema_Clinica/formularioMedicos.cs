﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Clinica
{
    public partial class FormularioMedicos : Form
    {

        string ID_Medico;
        public FormularioMedicos(string DNI)
        {
            InitializeComponent();
            ID_Medico = CRUD.getMedicoByDNI(DNI).ID_Medico.ToString(); //Parseamos el ID del medico
        }

        
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormularioMedicos_Load(object sender, EventArgs e)
        {
            foreach (DataRow data in CRUD.getTurnosMedico(ID_Medico))
            {
                //Ponemos en negrita los días que tengan turnos.
                mthCalendario.AddBoldedDate(DateTime.Parse(data["Fecha Turno"].ToString()));
            }
            //Refrescamos la lista de días en negrita
            mthCalendario.UpdateBoldedDates();
            DateRangeEventArgs rango = new DateRangeEventArgs(DateTime.Today, DateTime.Today);
            mthCalendario_DateSelected(sender, rango);
        }

        private void mthCalendario_DateSelected(object sender, DateRangeEventArgs e)
        {
            dgrTurnos.DataSource = CRUD.getTurnosMedicoByDate(ID_Medico, e.Start);
            dgrTurnos.Columns["ID_Paciente"].Visible = false;
            dgrTurnos.Columns["ID_Turno"].Visible = false;
        }

        private void btnTurnosAnteriores_Click(object sender, EventArgs e)
        {
            if (dgrTurnos.CurrentRow != null)
            {
                try
                {
                    //Parseamos el ID del paciente (de la columna inviisble ID_Paciente)
                    string ID_Paciente = dgrTurnos.CurrentRow.Cells["ID_Paciente"].Value.ToString();
                    //Seteamos el DataSource con los turnos del paciente seleccionado
                    dgrPacientes.DataSource = CRUD.getTurnosPaciente(ID_Paciente);
                    //Enfocamos la segunda pestaña
                    tabFormMedicos.SelectedIndex = 1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un turno de la lista.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormularioMedicos_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            { e.Cancel = true; }
        }
    }
}
