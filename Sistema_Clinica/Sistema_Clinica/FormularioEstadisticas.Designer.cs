﻿namespace Sistema_Clinica
{
    partial class FormularioEstadisticas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btnSalir = new System.Windows.Forms.Button();
            this.crtGraficoTurnos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dteFechaInicial = new System.Windows.Forms.DateTimePicker();
            this.lblFechaInicial = new System.Windows.Forms.Label();
            this.lblFechaFinal = new System.Windows.Forms.Label();
            this.dteFechaFinal = new System.Windows.Forms.DateTimePicker();
            this.btnConsulta = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.crtGraficoTurnos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Location = new System.Drawing.Point(829, 289);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(95, 23);
            this.btnSalir.TabIndex = 1;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // crtGraficoTurnos
            // 
            this.crtGraficoTurnos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.crtGraficoTurnos.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.crtGraficoTurnos.Legends.Add(legend1);
            this.crtGraficoTurnos.Location = new System.Drawing.Point(13, 13);
            this.crtGraficoTurnos.Name = "crtGraficoTurnos";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Turnos Concurridos";
            this.crtGraficoTurnos.Series.Add(series1);
            this.crtGraficoTurnos.Size = new System.Drawing.Size(810, 300);
            this.crtGraficoTurnos.TabIndex = 2;
            // 
            // dteFechaInicial
            // 
            this.dteFechaInicial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dteFechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dteFechaInicial.Location = new System.Drawing.Point(829, 29);
            this.dteFechaInicial.Name = "dteFechaInicial";
            this.dteFechaInicial.Size = new System.Drawing.Size(95, 20);
            this.dteFechaInicial.TabIndex = 3;
            // 
            // lblFechaInicial
            // 
            this.lblFechaInicial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaInicial.AutoSize = true;
            this.lblFechaInicial.Location = new System.Drawing.Point(829, 13);
            this.lblFechaInicial.Name = "lblFechaInicial";
            this.lblFechaInicial.Size = new System.Drawing.Size(67, 13);
            this.lblFechaInicial.TabIndex = 4;
            this.lblFechaInicial.Text = "Fecha Inicial";
            // 
            // lblFechaFinal
            // 
            this.lblFechaFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaFinal.AutoSize = true;
            this.lblFechaFinal.Location = new System.Drawing.Point(829, 52);
            this.lblFechaFinal.Name = "lblFechaFinal";
            this.lblFechaFinal.Size = new System.Drawing.Size(62, 13);
            this.lblFechaFinal.TabIndex = 6;
            this.lblFechaFinal.Text = "Fecha Final";
            // 
            // dteFechaFinal
            // 
            this.dteFechaFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dteFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dteFechaFinal.Location = new System.Drawing.Point(829, 68);
            this.dteFechaFinal.Name = "dteFechaFinal";
            this.dteFechaFinal.Size = new System.Drawing.Size(95, 20);
            this.dteFechaFinal.TabIndex = 5;
            // 
            // btnConsulta
            // 
            this.btnConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsulta.Location = new System.Drawing.Point(829, 260);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(95, 23);
            this.btnConsulta.TabIndex = 7;
            this.btnConsulta.Text = "Consultar";
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            // 
            // FormularioEstadisticas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 322);
            this.Controls.Add(this.btnConsulta);
            this.Controls.Add(this.lblFechaFinal);
            this.Controls.Add(this.dteFechaFinal);
            this.Controls.Add(this.lblFechaInicial);
            this.Controls.Add(this.dteFechaInicial);
            this.Controls.Add(this.crtGraficoTurnos);
            this.Controls.Add(this.btnSalir);
            this.MinimumSize = new System.Drawing.Size(944, 349);
            this.Name = "FormularioEstadisticas";
            this.Text = "Estadísticas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormularioEstadisticas_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.crtGraficoTurnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.DataVisualization.Charting.Chart crtGraficoTurnos;
        private System.Windows.Forms.DateTimePicker dteFechaInicial;
        private System.Windows.Forms.Label lblFechaInicial;
        private System.Windows.Forms.Label lblFechaFinal;
        private System.Windows.Forms.DateTimePicker dteFechaFinal;
        private System.Windows.Forms.Button btnConsulta;
    }
}