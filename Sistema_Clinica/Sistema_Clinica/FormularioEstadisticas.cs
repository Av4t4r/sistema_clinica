﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Clinica
{
    public partial class FormularioEstadisticas : Form
    {
        public FormularioEstadisticas()
        {
            InitializeComponent();
        }

        private void FormularioEstadisticas_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            { e.Cancel = true; }
        }

        private void actualizarFormulario (DateTime fechaInicial, DateTime fechaFinal)
        {
            foreach (var series in crtGraficoTurnos.Series)
            {
                series.Points.Clear();
            }
            int prescencias = 0;
            int ausencias = 0;
            int total = 0;
            foreach (DataRow fila in CRUD.getTurnos(fechaInicial, fechaFinal))
            {
                if (fila["Asistencia"].Equals(true))
                {
                    prescencias += 1;
                    total += 1;
                }
                else
                {
                    ausencias += 1;
                    total += 1;
                }
            }
            crtGraficoTurnos.Series[0].Points.Add(prescencias);
            crtGraficoTurnos.Series[0].Points.Add(ausencias);
            crtGraficoTurnos.Series[0].Points[0].LegendText = "Turnos Concurridos";
            crtGraficoTurnos.Series[0].Points[1].LegendText = "Turnos Ausentes";
            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {
            actualizarFormulario(dteFechaInicial.Value, dteFechaFinal.Value);
        }
    }
}
