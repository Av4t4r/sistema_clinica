﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Sistema_Clinica
{
    public static class HashPass
    {
        static UnicodeEncoding ByteConverter = new UnicodeEncoding();
        static CspParameters cspParams = new CspParameters();
        static RSACryptoServiceProvider rsa;

        public static void generarRSAPair ()
        {
            cspParams.KeyContainerName = "Sistema_Clinica_CSP";
            rsa = new RSACryptoServiceProvider(cspParams);
            try
            {
                File.WriteAllText(Directory.GetCurrentDirectory() + "\\publickey.xml", rsa.ToXmlString(false), Encoding.UTF8);
                File.WriteAllText(Directory.GetCurrentDirectory() + "\\privatekey.xml", rsa.ToXmlString(true), Encoding.UTF8);
            }
            catch
            {
                throw new UnauthorizedAccessException ("No se puede crear el archivo de encriptación, verifique sus permisos");
            }
        }

        public static string encriptarContraseña(string contraseña)
        {
            if (contraseña == String.Empty)
            {
                throw new ContraseñaNoRespetaFormato("La contraseña no puede estar vacía");
            }
            SHA256 sha = new SHA256CryptoServiceProvider();
            //calcular el hash en base a la contraseña
            sha.ComputeHash(Encoding.UTF8.GetBytes(contraseña));
            //El resultado es un array de bytes
            byte[] resultado = sha.Hash;
            //Que convertimos a un string
            StringBuilder strBuilder = new StringBuilder();
            foreach (Byte b in resultado)
                strBuilder.Append(b.ToString("x2"));
            return strBuilder.ToString();
        }


        public static string RSAEncrypt(string DataToEncrypt, bool DoOAEPPadding)
        {
            //se instancian los parametros de Cryptography Service Provider
            //Con un KeyContainer para guardar las claves y que sean reutilizables
            cspParams.KeyContainerName = "Sistema_Clinica_CSP";
            try
            {
                byte[] DataToEncryptArray = ByteConverter.GetBytes(DataToEncrypt);
                byte[] encryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(cspParams))
                {
                    //Se importan los parametros necesarios
                    //Para encriptar solo se necesita la clave pública
                    RSA.FromXmlString(File.ReadAllText(Directory.GetCurrentDirectory() + "\\publickey.xml"));

                    //Se encripta el byte array. El OAEPP padding debería andar en XP
                    //Para adelante, pero fallaba por algún motivo
                    encryptedData = RSA.Encrypt(DataToEncryptArray, DoOAEPPadding);
                }
                return Convert.ToBase64String(encryptedData);
            }
            //Se atrapan las excepciones de criptografía
            //Por lo general son "invalid parameter" o similares
            catch (CryptographicException)
            {
                throw new CryptographicException("Ocurrio un error al intentar encriptar la información. ¿El archivo de encriptación tuvo que ser re-generado?");
            }

        }


        public static string RSADecrypt(string DataToDecrypt, bool DoOAEPPadding)
        {
            //se instancian los parametros de Cryptography Service Provider
            //Con un KeyContainer para guardar las claves y que sean reutilizables
            cspParams.KeyContainerName = "Sistema_Clinica_CSP";
            try
            {
                byte[] DataToDecryptArray = Convert.FromBase64String(DataToDecrypt);
                byte[] decryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    //Se importan los parámetros
                    //Para desencriptar se necesita la clave privada

                    RSA.FromXmlString(File.ReadAllText(Directory.GetCurrentDirectory() + "\\privatekey.xml")); 

                    //RSA.FromXmlString(Directory.GetCurrentDirectory() + "\\privatekey.xml");

                    //Se decripta el byte array y se indica si usa OAEPP o no
                    //Se supone que de XP en adelante anda, pero da error
                    decryptedData = RSA.Decrypt(DataToDecryptArray, DoOAEPPadding);
                }
                return ByteConverter.GetString(decryptedData);
            }
            //Se atrapan las excepciones de criptografía
            //Por lo general son "invalid parameter" o similares   
            catch (CryptographicException)
            {
                throw new CryptographicException("Ocurrio un error al intentar desencriptar la información. ¿El archivo de encriptación tuvo que ser re-generado?");
            }

        }
    }
}
