﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Clinica
{
    public partial class FormularioOperarios : Form
    {
        string ID_Operario;
        public FormularioOperarios(string DNI)
        {
            InitializeComponent();
            ID_Operario = CRUD.getOperarioByDNI(DNI).ID_Operario.ToString(); //Parseamos el ID del operario
            actualizarDataGrids(); //actualizarDataGrids viene acá en lugar de Form_Load para no perder el foco y seleccionar el primer paciente y médico que pueda de las dataGrids
        }

        private void actualizarDataGrids()
        {
            //Llenamos las dataGrids
            dgrListaMedicos.DataSource = CRUD.getMedicos();
            dgrListaPacientes.DataSource = CRUD.getPacientes();
            dgrPacientes.DataSource = CRUD.getPacientes();
            //Hacemos invisibles las columnas que no necesitamos y/o no queremos mostrar
            dgrListaMedicos.Columns["ID_Medico"].Visible = false;
            dgrListaMedicos.Columns["DNI"].Visible = false;
            dgrListaMedicos.Columns["Fecha_Nacimiento"].Visible = false;
            dgrListaMedicos.Columns["Domicilio"].Visible = false;
            dgrListaMedicos.Columns["Telefono"].Visible = false;
            dgrListaMedicos.Columns["Actividad"].Visible = false;
            dgrListaMedicos.Columns["Observaciones"].Visible = false;
            dgrListaMedicos.Columns["Contraseña"].Visible = false;
            dgrListaMedicos.Columns["Nro_Matricula"].Visible = false;
            dgrListaPacientes.Columns["ID_Paciente"].Visible = false;
            dgrListaPacientes.Columns["DNI"].Visible = false;
            dgrListaPacientes.Columns["Fecha_Nacimiento"].Visible = false;
            dgrListaPacientes.Columns["Domicilio"].Visible = false;
            dgrListaPacientes.Columns["Telefono"].Visible = false;
            dgrListaPacientes.Columns["Actividad"].Visible = false;
            dgrListaPacientes.Columns["ID_Operario"].Visible = false;
            dgrPacientes.Columns["ID_Paciente"].Visible = false;
            dgrPacientes.Columns["ID_Operario"].Visible = false;
            dgrPacientes.Columns["Actividad"].Visible = false;
            dgrPacientes.Columns["Fecha_Nacimiento"].HeaderText = "Fecha Nacimiento";
        }

        private void FormularioOperarios_Load(object sender, EventArgs e)
        {
        }

        private void FormularioOperarios_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            { e.Cancel = true; }
        }

        #region 1ra pestaña
        private void txtBusquedaMedico_TextChanged(object sender, EventArgs e)
        {
            if (txtBusquedaMedico.Text != "")
            {
                btnBuscarMedico.Enabled = true;
            }
            else
            {
                btnBuscarMedico.Enabled = false;
            }
        }

        private void dgrListaMedicos_SelectionChanged(object sender, EventArgs e)
        {
            if (dgrListaMedicos.CurrentRow != null)
            {
                dgrTurnosPorMedico.DataSource = CRUD.getTurnosMedico(dgrListaMedicos.CurrentRow.Cells["ID_Medico"].Value.ToString());
                dgrTurnosPorMedico.Columns["ID_Turno"].Visible = false;
                dgrTurnosPorMedico.Columns["ID_Paciente"].Visible = false;
                dgrTurnosPorMedico.Columns["Actividad"].Visible = false;
            }
            else { }
        }

        private void btnBuscarMedico_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow fila in dgrListaMedicos.Rows)
            {
                if (fila.Cells["Apellido"].Value.ToString().Equals(txtBusquedaMedico.Text))
                {
                    fila.Selected = true;
                    break;
                }
            }
        }

        private void btnLimpiarMedicos_Click(object sender, EventArgs e)
        {
            actualizarDataGrids();
            txtBusquedaMedico.Text = String.Empty;
        }

        private void txtBusquedaPaciente_TextChanged(object sender, EventArgs e)
        {
            if (txtBusquedaPaciente.Text != "")
            {
                btnBuscarPaciente.Enabled = true;
            }
            else
            {
                btnBuscarPaciente.Enabled = false;
            }
        }

        private void btnLimpiarPacientes_Click(object sender, EventArgs e)
        {
            actualizarDataGrids();
            txtBusquedaPaciente.Text = String.Empty;
        }

        private void btnBuscarPaciente_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow fila in dgrListaPacientes.Rows)
            {
                if (fila.Cells["Apellido"].Value.ToString().Equals(txtBusquedaPaciente.Text))
                {
                    fila.Selected = true;
                    break;
                }
            }
        }

        private void btnTurnoNuevo_Click(object sender, EventArgs e)
        {
            if (dgrListaPacientes.CurrentRow != null && dgrListaMedicos.CurrentRow != null)
            {
                string ID_Paciente = dgrListaPacientes.CurrentRow.Cells["ID_Paciente"].Value.ToString();
                string ID_Medico = dgrListaMedicos.CurrentRow.Cells["ID_Medico"].Value.ToString();
                FormularioTurnoNuevo turnoNuevo = new FormularioTurnoNuevo(ID_Operario, ID_Paciente, ID_Medico);
                turnoNuevo.ShowDialog();
                turnoNuevo.Dispose();
                actualizarDataGrids();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un paciente y un medico", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCambiarTurno_Click(object sender, EventArgs e)
        {
            if (dgrTurnosPorMedico.CurrentRow != null)
            {
                string ID_Turno = dgrTurnosPorMedico.CurrentRow.Cells["ID_Turno"].Value.ToString();
                DateTime fecha = DateTime.Parse(dgrTurnosPorMedico.CurrentRow.Cells["Fecha Turno"].Value.ToString());
                string horario = dgrTurnosPorMedico.CurrentRow.Cells["Horario Turno"].Value.ToString();
                string motivo = dgrTurnosPorMedico.CurrentRow.Cells["Motivo Consulta"].Value.ToString();
                bool actividad = (Boolean)dgrTurnosPorMedico.CurrentRow.Cells["Actividad"].Value;
                bool asistencia = (Boolean)dgrTurnosPorMedico.CurrentRow.Cells["Asistencia"].Value;
                string observaciones = dgrTurnosPorMedico.CurrentRow.Cells["Observaciones"].Value.ToString();
                FormularioTurnoEditar turnoEditar = new FormularioTurnoEditar(ID_Turno, fecha, horario, motivo, observaciones, actividad, asistencia);
                turnoEditar.ShowDialog();
                turnoEditar.Dispose();
                actualizarDataGrids();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un turno para editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 2da pestaña
        private void btnSalir2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region (des)habilitar boton nuevo paciente)
        private void txtApellido_TextChanged(object sender, EventArgs e)
        {
            if (txtApellido.Text != "" && txtNombre.Text != "" && txtDNI.Text != "" && txtTelefono.Text != "" && txtDomicilio.Text != "")
            { btnNuevoPaciente.Enabled = true; }
            else
            { btnNuevoPaciente.Enabled = false; }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            if (txtApellido.Text != "" && txtNombre.Text != "" && txtDNI.Text != "" && txtTelefono.Text != "" && txtDomicilio.Text != "")
            { btnNuevoPaciente.Enabled = true; }
            else
            { btnNuevoPaciente.Enabled = false; }
        }

        private void txtDNI_TextChanged(object sender, EventArgs e)
        {
            if (txtApellido.Text != "" && txtNombre.Text != "" && txtDNI.Text != "" && txtTelefono.Text != "" && txtDomicilio.Text != "")
            { btnNuevoPaciente.Enabled = true; }
            else
            { btnNuevoPaciente.Enabled = false; }
        }

        private void txtTelefono_TextChanged(object sender, EventArgs e)
        {
            if (txtApellido.Text != "" && txtNombre.Text != "" && txtDNI.Text != "" && txtTelefono.Text != "" && txtDomicilio.Text != "")
            { btnNuevoPaciente.Enabled = true; }
            else
            { btnNuevoPaciente.Enabled = false; }
        }

        private void txtDomicilio_TextChanged(object sender, EventArgs e)
        {
            if (txtApellido.Text != "" && txtNombre.Text != "" && txtDNI.Text != "" && txtTelefono.Text != "" && txtDomicilio.Text != "")
            { btnNuevoPaciente.Enabled = true; }
            else
            { btnNuevoPaciente.Enabled = false; }
        }
        #endregion
        private void btnNuevoPaciente_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(CRUD.nuevoPaciente(txtApellido.Text, txtNombre.Text, txtDNI.Text, dteFechaNacimiento.Value, txtDomicilio.Text, txtTelefono.Text, txtObservaciones.Text, ID_Operario), 
                    "Operación completa.", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                txtNombre.Text = String.Empty; txtApellido.Text = String.Empty; txtDNI.Text = String.Empty; txtDomicilio.Text = String.Empty; txtTelefono.Text = String.Empty; 
                txtObservaciones.Text = String.Empty;
                actualizarDataGrids();
            }
            catch (ArgumentNullException ex)
            {MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);}
            catch  (FormatException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (PacienteExistente ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (DatosPersonalesVacios ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnEditarPaciente_Click(object sender, EventArgs e)
        {
            if (dgrPacientes.CurrentRow != null)
            {
                btnGuardarCambios.Enabled = true;
                btnNuevoPaciente.Enabled = false;
                btnCancelarCambios.Enabled = true;
                txtApellido.Text = dgrPacientes.CurrentRow.Cells["Apellido"].Value.ToString();
                txtNombre.Text = dgrPacientes.CurrentRow.Cells["Nombre"].Value.ToString();
                txtDNI.Text = dgrPacientes.CurrentRow.Cells["DNI"].Value.ToString();
                txtTelefono.Text = dgrPacientes.CurrentRow.Cells["Telefono"].Value.ToString();
                txtDomicilio.Text = dgrPacientes.CurrentRow.Cells["Domicilio"].Value.ToString();
                dteFechaNacimiento.Value = DateTime.Parse(dgrPacientes.CurrentRow.Cells["Fecha_Nacimiento"].Value.ToString());
                txtObservaciones.Text = dgrPacientes.CurrentRow.Cells["Observaciones"].Value.ToString();
            }
            else
            { MessageBox.Show("Debe elegir un paciente para editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnGuardarCambios_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(CRUD.editarPaciente(dgrPacientes.CurrentRow.Cells["ID_Paciente"].Value.ToString(), txtApellido.Text, txtNombre.Text, txtDNI.Text, dteFechaNacimiento.Value, 
                    txtDomicilio.Text, txtTelefono.Text, txtObservaciones.Text), "Operación exitosa", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                txtNombre.Text = String.Empty; txtApellido.Text = String.Empty; txtDNI.Text = String.Empty; txtDomicilio.Text = String.Empty; txtTelefono.Text = String.Empty;
                txtObservaciones.Text = String.Empty; btnCancelarCambios.Enabled = false; btnGuardarCambios.Enabled = false;
                actualizarDataGrids();
            }
            catch (ArgumentNullException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (DNIEnUso ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (FormatException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (PacienteNoExistente ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (OverflowException ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (DatosPersonalesVacios ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (FechaFueraDeRango ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnCancelarCambios_Click(object sender, EventArgs e)
        {
            txtNombre.Text = String.Empty; txtApellido.Text = String.Empty; txtDNI.Text = String.Empty; txtDomicilio.Text = String.Empty; txtTelefono.Text = String.Empty;
            txtObservaciones.Text = String.Empty; btnCancelarCambios.Enabled = false; btnGuardarCambios.Enabled = false;
            actualizarDataGrids();
        }
        #endregion
    }
}

